<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */
 $route['translate_uri_dashes'] = FALSE;

 $route['default_controller'] = "welcome";
 $route['test'] = "test";
//$route['404_override'] = '';
 
$route['api'] = "api";
$route['api/Staff_Login/'] = "api/Staff_Login/";
$route['api/Parent_Login/'] = "api/Parent_Login/";
$route['api/Staff_ForgotPassword/'] = "api/Staff_ForgotPassword/";
$route['api/Parent_ForgotPasssword/'] = "api/Parent_ForgotPasssword/";
$route['api/Staff_GetAllCenters/'] = "api/Staff_GetAllCenters/";
$route['api/Staff_GetAllClasses/'] = "api/Staff_GetAllClasses/";
$route['api/Staff_GetAllChildren/'] = "api/Staff_GetAllChildren/";
$route['api/Staff_GetSpecialRequests/(:num)/'] = 'api/Staff_GetSpecialRequests/$1/';
$route['api/Staff_UploadPortolioPhoto/'] = 'api/Staff_UploadPortolioPhoto/';
$route['api/Staff_UploadFiles/'] = 'api/Staff_UploadFiles/';
$route['api/Staff_DeleteFile/'] = 'api/Staff_DeleteFile/';
$route['api/Get_All_Staffs/'] = 'api/Get_All_Staffs/';
//$route['api/requestdetails/'] = 'api/requestdetails/';
//$route['api/verifyrequests/'] = 'api/verifyrequests/';
$route['api/Staff_Logout/'] = 'api/Staff_Logout/';
$route['api/Parent_Logout/'] = 'api/Parent_Logout/';
$route['api/Parent_GetAnnouncementListing/'] = 'api/Parent_GetAnnouncementListing/';
$route['api/Staff_GetAnnouncementListing/(:num)/'] = 'api/Staff_GetAnnouncementListing/$1/';
$route['api/Staff_AnnouncementCategory/'] = 'api/Staff_AnnouncementCategory/';
$route['api/Staff_CreateAnnouncement/'] = 'api/Staff_CreateAnnouncement/';
$route['api/Staff_SendReplyForSpecialRequest/'] = 'api/Staff_SendReplyForSpecialRequest/';
$route['api/Staff_Feedback/'] = 'api/Staff_Feedback/';
$route['api/Staff_TermsAndConditions/'] = 'api/Staff_TermsAndConditions/';
$route['api/About_StaffApp/'] = 'api/About_StaffApp/';
$route['api/Staff_GetStudentProfileAndCheckin/'] = 'api/Staff_GetStudentProfileAndCheckin/';
$route['api/Staff_StudentCheckout/'] = 'api/Staff_StudentCheckout/';



$route['api/Parent_EditProfile/']= "api/Parent_EditProfile/";
$route['api/Parent_Feedback/']= "api/Parent_Feedback/";
$route['api/Parent_GetPortfolio/']= "api/Parent_GetPortfolio/";
$route['api/Parent_CreateNewRequest_Others/']= "api/Parent_CreateNewRequest_Others/";
$route['api/Parent_GetOthersRequests/']= "api/Parent_GetOthersRequests/";
$route['api/Parent_CreateNewRequest_Medications/']= "api/Parent_CreateNewRequest_Medications/";
$route['api/Parent_GetMedicationrequests/']= "api/Parent_GetMedicationrequests/";
$route['api/Parent_GetRequestCount/']= "api/Parent_GetRequestCount/";
$route['api/Parent_GetAttendance/']= "api/Parent_GetAttendance/";
//$route['api/staffportfolio/(:any)'] = 'api/staffportfolio/$1/';




/* End of file routes.php */
/* Location: ./application/config/routes.php */