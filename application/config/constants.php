<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


define('ROOT', $_SERVER['DOCUMENT_ROOT']);

define('SITE_TITLE', 'childhaven');
define('SITE_KEY_WORDS', 'childhaven');
define('SITE_DESCRIPTION', 'childhaven');



if ($_SERVER['HTTP_HOST'] == "local.childhaven.com") {
    define('ASSETS', '/assets/');
    define('UPLOAD_DIR', ROOT . '/assets/upload/');
    define('VIEW_DIR', '/assets/upload/');
} else if ($_SERVER['HTTP_HOST'] == "smeitapplications.com") {
    define('ASSETS', 'chv2/assets/');
    define('UPLOAD_DIR', ROOT . '/chv2/assets/upload/');
    define('VIEW_DIR', 'chv2/assets/upload/');
} else {
    define('ASSETS', '/kindergardens/assets/');
    define('UPLOAD_DIR', ROOT . '/kindergardens/assets/uploads/');
    define('VIEW_DIR', '/kindergardens/assets/uploads/');
}
//echo UPLOAD_DIR;
define('STAFF_PHOTO_PATH', UPLOAD_DIR . 'staff/');
define('STAFF_PHOTO_VIEW_PATH', VIEW_DIR . 'staff/');
define('STAFF_PHOTO_THUMB_PATH',STAFF_PHOTO_PATH."thumb/");

define('PARENT_PHOTO_PATH', UPLOAD_DIR . 'parent/');
define('PARENT_PHOTO_VIEW_PATH', VIEW_DIR . 'parent/');
define('PARENT_PHOTO_THUMB_PATH',PARENT_PHOTO_PATH."thumb/");

define('CHILD_LEARNING_PORTFOLIO_UPLOAD',UPLOAD_DIR."childlearning_portfolio/");
define('CHILD_LEARNING_PORTFOLIO_WEB',VIEW_DIR."childlearning_portfolio/");

define('CHILD_LEARNING_PORTFOLIO_THUMB_UPLOAD',UPLOAD_DIR."childlearning_portfolio/thumb/");
define('CHILD_LEARNING_PORTFOLIO_THUMB_WEB',VIEW_DIR."childlearning_portfolio/thumb/");

define('CHILD_UPLOAD',UPLOAD_DIR."child/");
define('CHILD_WEB',VIEW_DIR."child/");

define('CHILD_THUMB_UPLOAD',UPLOAD_DIR."child/thumb/");
define('CHILD_THUMB_WEB',VIEW_DIR."child/thumb/");

define('LIBRARY_DOCUMENT_PATH',UPLOAD_DIR.'library/'); 
define('LIBRARY_DOCUMENT_VIEW_PATH',VIEW_DIR.'library/');

/* End of file constants.php */
/* Location: ./application/config/constants.php */