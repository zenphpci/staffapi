<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    //protected $_name = 'tbl_countries';
    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }

    public function getStaff($data) {
        $sql = "SELECT * 
		 		FROM tbl_staff t1 inner join tbl_centre t2 on t2.centre_id = t1.centre_id
				WHERE staff_login_id 	   = '" . $data["email"] . "'
				AND	  staff_password 	   = '" . $data["password"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }
    public function getParent($data) {
        $sql = "SELECT * 
		 		FROM tbl_parent t1 inner join tbl_centre t2 on t2.centre_id = t1.centre_id 
                                inner join tbl_child t3 on t3.child_id = t1.child_id
				WHERE login_id 	   = '" . $data["email"] . "'
				AND password = '" . $data["password"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }

    public function updateStaff($data) {
        $sql = "Update tbl_staff SET staff_login_status='1',devicetype = '" . $data["devicetype"] . "',devicetoken = '" . $data["devicetoken"] . "'
				WHERE staff_login_id = '" . $data["email"] . "'";
//         $sql = "Update tbl_staff SET devicetype = 'asd',devicetoken = 'sadasdas'
//				WHERE staff_login_id = '" . $data["email"] . "'";

        $query = $this->db->query($sql);
        if (!$query) {
            return 'failure';
        }
//        return $rset_get_staff[0];
//        return 1;
    }
    public function updateParent($data) {
        $sql = "Update tbl_parent SET parent_login_status='1',devicetype = '" . $data["devicetype"] . "',devicetoken = '" . $data["devicetoken"] . "'
				WHERE login_id = '" . $data["email"] . "'";
//         $sql = "Update tbl_staff SET devicetype = 'asd',devicetoken = 'sadasdas'
//				WHERE staff_login_id = '" . $data["email"] . "'";

        $query = $this->db->query($sql);
        if (!$query) {
            return 'failure';
        }
//        return $rset_get_staff[0];
//        return 1;
    }

    public function getStaffbyid($data) {
        $sql = "SELECT * 
		 		FROM tbl_staff
				WHERE staff_id 	= '" . $data["staffid"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }
    public function getParentbyid($data) {
        $sql = "SELECT * 
		 		FROM tbl_parent
				WHERE parent_id 	= '" . $data["staffid"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }

    public function getStaffbyemailid($data) {
        $sql = "SELECT * 
		 		FROM tbl_staff
				WHERE staff_login_id 	= '" . $data["staffmail"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }
    public function getParentbyemailid($data) {
        $sql = "SELECT * 
		 		FROM tbl_parent
				WHERE login_id 	= '" . $data["staffmail"] . "'
				";
        $rset_get_staff = $this->db->query($sql)->result_array();
        return $rset_get_staff[0];
    }

    public function getStaffportfolio($data) {
        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {



            $sql = "SELECT
						tbl_clp_selected_child.*,
						tbl_child_learning_portfolio.*,
						tbl_child_learning_portfolio.portfolio_id as portfolio_id,
						centre_name,
						staff.staff_name,
						child_display_id,tbl_child.child_id,
						child_name,
						class_title,
						pies_name,GROUP_CONCAT(tbl_clp_selected_child.child_id SEPARATOR ', ') as childis
					FROM   tbl_clp_selected_child
					LEFT  JOIN tbl_child_learning_portfolio ON tbl_clp_selected_child.portfolio_id = tbl_child_learning_portfolio.portfolio_id 
					LEFT  JOIN tbl_staff as staff 			ON staff.staff_id  		= tbl_child_learning_portfolio.portfolio_updated_by_id
					LEFT  JOIN tbl_centre 		 		 	ON tbl_child_learning_portfolio.centre_id = tbl_centre.centre_id
					LEFT  JOIN tbl_child					ON tbl_child.child_id  	= tbl_clp_selected_child.child_id
					LEFT  JOIN tbl_pies						ON tbl_pies.id  		= tbl_child_learning_portfolio.pies_id
					LEFT  JOIN tbl_class					ON tbl_class.class_id  	= tbl_child_learning_portfolio.class_id
					
GROUP BY tbl_clp_selected_child.portfolio_id 
					ORDER  BY tbl_child_learning_portfolio.portfolio_id desc";
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT
						tbl_clp_selected_child.*,
						tbl_child_learning_portfolio.*,
						tbl_child_learning_portfolio.portfolio_id as portfolio_id,
						centre_name,
						staff.staff_name,
						child_display_id,tbl_child.child_id,
						child_name,
						class_title,
						pies_name,GROUP_CONCAT(tbl_clp_selected_child.child_id SEPARATOR ', ') as childis
						
					FROM   tbl_clp_selected_child
					LEFT  JOIN tbl_child_learning_portfolio ON tbl_clp_selected_child.portfolio_id = tbl_child_learning_portfolio.portfolio_id 
					LEFT  JOIN tbl_staff as staff 			ON staff.staff_id  		= tbl_child_learning_portfolio.portfolio_updated_by_id
					LEFT  JOIN tbl_centre 		 		 	ON tbl_child_learning_portfolio.centre_id = tbl_centre.centre_id
					LEFT  JOIN tbl_child					ON tbl_child.child_id  	= tbl_clp_selected_child.child_id
					LEFT  JOIN tbl_pies						ON tbl_pies.id  		= tbl_child_learning_portfolio.pies_id
					LEFT  JOIN tbl_class					ON tbl_class.class_id  	= tbl_child_learning_portfolio.class_id
					GROUP BY tbl_clp_selected_child.portfolio_id 
					ORDER  BY tbl_child_learning_portfolio.portfolio_id desc LIMIT $limit";
        } else if ($start != '' && $limit != '') {
            $sql = "SELECT
						tbl_clp_selected_child.*,
						tbl_child_learning_portfolio.*,
						tbl_child_learning_portfolio.portfolio_id as portfolio_id,
						centre_name,
						staff.staff_name,
						child_display_id,tbl_child.child_id,
						child_name,
						class_title,
						pies_name,GROUP_CONCAT(tbl_clp_selected_child.child_id SEPARATOR ', ') as childis
						
					FROM   tbl_clp_selected_child
					LEFT  JOIN tbl_child_learning_portfolio ON tbl_clp_selected_child.portfolio_id = tbl_child_learning_portfolio.portfolio_id 
					LEFT  JOIN tbl_staff as staff 			ON staff.staff_id  		= tbl_child_learning_portfolio.portfolio_updated_by_id
					LEFT  JOIN tbl_centre 		 		 	ON tbl_child_learning_portfolio.centre_id = tbl_centre.centre_id
					LEFT  JOIN tbl_child					ON tbl_child.child_id  	= tbl_clp_selected_child.child_id
					LEFT  JOIN tbl_pies						ON tbl_pies.id  		= tbl_child_learning_portfolio.pies_id
					LEFT  JOIN tbl_class					ON tbl_class.class_id  	= tbl_child_learning_portfolio.class_id
					GROUP BY tbl_clp_selected_child.portfolio_id 
					ORDER  BY tbl_child_learning_portfolio.portfolio_id desc LIMIT $start,$limit";
        }

        $query = $this->db->query($sql);

        $data = array();
        $data["portfolio_list"] = $query->result_array();
        $data["total_portfolio"] = $query->num_rows();
        return $data;
    }

    public function getParentPortfolio($data) {
      
       $sql = "SELECT port.*, det.*, pfl.*, GROUP_CONCAT(pfl.file_img ORDER BY det.portfolio_files)as file_ame,cen.centre_name, cls.class_title FROM tbl_child_learning_portfolio as port 
        INNER JOIN tbl_clp_selected_child det ON det.portfolio_id = port.portfolio_id
        INNER JOIN tbl_portfolio_file pfl ON FIND_IN_SET(pfl.file_id, det.portfolio_files) > 0
        INNER JOIN tbl_centre cen ON cen.centre_id = port.centre_id
        INNER JOIN tbl_parent par ON par.child_id = det.child_id
        INNER JOIN tbl_class cls ON cls.class_id = port.class_id
        WHERE det.is_send_to_parent='1'";
            $data = array();
        $query = $this->db->query($sql);
            if(! $query){
                return false;
            }else{
                $data["portfolio_list"] = $query->result_array();
                $data["total_portfolio"] = $query->num_rows();
                return $data;
            }
    }

    public function getallcenters($data) {
        $sql = "SELECT * from tbl_centre";

        $query = $this->db->query($sql);

        $data = array();
        $data["centre_list"] = $query->result_array();
        $data["total_centre"] = $query->num_rows();
        return $data;
    }

    public function getallclasses($data) {
        $sql = "SELECT class_id,class_title from tbl_class where center_id = '" . $data["centreid"] . "'";

        $query = $this->db->query($sql);

        $data = array();
        $data["classes_list"] = $query->result_array();
        $data["total_classes"] = $query->num_rows();
        return $data;
    }

    public function getallchildrens($data) {
        $sql = "SELECT child_id,child_name,child_photo from tbl_child where class_id = '" . $data["classid"] . "'";

        $query = $this->db->query($sql);

        $data = array();
        $data["child_list"] = $query->result_array();
        $data["total_childrens"] = $query->num_rows();
        return $data;
    }
    public function getaannouncemetcat($data) {
        $sql = "SELECT event_catid,event_title from tbl_event_category where event_status = 1 ";

        $query = $this->db->query($sql);

        $data = array();
        $data["category_list"] = $query->result_array();
        $data["total_category"] = $query->num_rows();
        return $data;
    }
    public function getstaffphoto($stfid) {
        $sql = "select staff_photo from tbl_staff where staff_id='$stfid'";
        $query = $this->db->query($sql);

        $data = array();
        $data["staff_list"] = $query->result_array();
      
        return $data;
    }

     public function getParentphoto($prntid) {
        $sql = "select photourl from tbl_parent where parent_id='$prntid'";
        $query = $this->db->query($sql);

        $data = array();
        $data["parent_list"] = $query->result_array();
      
        return $data;
    }


    public function getStaffrequests($data) {
        
        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {
            $sql = "SELECT csp.*, c.child_display_id, c.child_name, cen.centre_name, cls.class_title, stf.staff_name as special_request_updated_by_id
						FROM tbl_child_special_request AS csp 
						LEFT JOIN `tbl_child` AS c ON csp.child_id = c.child_id
						LEFT JOIN `tbl_centre` AS cen ON csp.centre_id = cen.centre_id
						LEFT JOIN `tbl_class` AS cls ON csp.class_id = cls.class_id
						LEFT JOIN `tbl_staff` AS stf ON csp.special_request_updated_by_id = stf.staff_id
						WHERE 1=1
						ORDER BY csp.special_request_id desc";
						
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT csp.*, c.child_display_id, c.child_name, cen.centre_name, cls.class_title, stf.staff_name as special_request_updated_by_id
						FROM tbl_child_special_request AS csp 
						LEFT JOIN `tbl_child` AS c ON csp.child_id = c.child_id
						LEFT JOIN `tbl_centre` AS cen ON csp.centre_id = cen.centre_id
						LEFT JOIN `tbl_class` AS cls ON csp.class_id = cls.class_id
						LEFT JOIN `tbl_staff` AS stf ON csp.special_request_updated_by_id = stf.staff_id
						WHERE 1=1
						ORDER BY csp.special_request_id desc LIMIT $limit";
        }else if($start != '' && $limit != ''){
            $sql = "SELECT csp.*, c.child_display_id, c.child_name, cen.centre_name, cls.class_title, stf.staff_name as special_request_updated_by_id
						FROM tbl_child_special_request AS csp 
						LEFT JOIN `tbl_child` AS c ON csp.child_id = c.child_id
						LEFT JOIN `tbl_centre` AS cen ON csp.centre_id = cen.centre_id
						LEFT JOIN `tbl_class` AS cls ON csp.class_id = cls.class_id
						LEFT JOIN `tbl_staff` AS stf ON csp.special_request_updated_by_id = stf.staff_id
						WHERE 1=1
						ORDER BY csp.special_request_id desc LIMIT $start,$limit";
        }
        
        
        $query = $this->db->query($sql);


        $data = array();
        $data["requests_list"] = $query->result_array();
        $data["total_requests"] = $query->num_rows();
        return $data;
    }

    public function getrequestsbyid($data) {

        $sql = "SELECT csp.*, c.child_display_id, c.child_name, cen.centre_name, cls.class_title, stf.staff_name as special_request_updated_by_id
						FROM tbl_child_special_request AS csp 
						LEFT JOIN `tbl_child` AS c ON csp.child_id = c.child_id
						LEFT JOIN `tbl_centre` AS cen ON csp.centre_id = cen.centre_id
						LEFT JOIN `tbl_class` AS cls ON csp.class_id = cls.class_id
						LEFT JOIN `tbl_staff` AS stf ON csp.special_request_updated_by_id = stf.staff_id
						WHERE csp.special_request_id='$data'
					";
        $query = $this->db->query($sql);


        $data = array();
        $data["requests_details"] = $query->result_array();
        $data["total_requests"] = $query->num_rows();
        return $data;
    }

    public function getannouncementsbyid($data) {

        $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_staff t2 on t2.staff_id = t1.announcement_updated_by_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid where t1.announcement_id='$data'";
        $query = $this->db->query($sql);


        $data = array();
        $data["announcement_details"] = $query->result_array();
        $data["total_announcemnets"] = $query->num_rows();
        return $data;
    }

    public function getStaffuploadfiles($data) {

        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {
            $sql = "SELECT * from tbl_library ORDER BY id desc";
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT * from tbl_library ORDER BY id desc LIMIT $limit";
        }else if($start != '' && $limit != ''){
            $sql = "SELECT * from tbl_library ORDER BY id desc LIMIT $start,$limit";
        }
//        echo $sql;
        $query = $this->db->query($sql);
        $data = array();
        $data["file_list"] = $query->result_array();
        $data["total_files"] = $query->num_rows();
        return $data;
    }
    public function getStaffAnnouncements($data) {

        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_staff t2 on t2.staff_id = t1.announcement_updated_by_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc";
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_staff t2 on t2.staff_id = t1.announcement_updated_by_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc LIMIT $limit";
        }else if($start != '' && $limit != ''){
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_staff t2 on t2.staff_id = t1.announcement_updated_by_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc LIMIT $start,$limit";
        }
//        echo $sql;
        $query = $this->db->query($sql);
        $data = array();
        $data["announcement_list"] = $query->result_array();
        $data["total_announcement"] = $query->num_rows();
        return $data;
    }
    public function getParentAnnouncements($data) {

        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_centre t2 on t2.centre_id = t1.centre_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc";
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_centre t2 on t2.centre_id = t1.centre_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc LIMIT $limit";
        }else if($start != '' && $limit != ''){
            $sql = "SELECT * from tbl_announcements t1 INNER JOIN tbl_centre t2 on t2.centre_id = t1.centre_id INNER JOIN tbl_event_category t3 on t3.event_catid = t1.event_catid ORDER BY t1.announcement_id desc LIMIT $start,$limit";
        }
//        echo $sql;
        $query = $this->db->query($sql);
        $data = array();
        if($query){
            $data["announcement_list"] = $query->result_array();
            $data["total_announcement"] = $query->num_rows();
            return $data;
        }else{

        }
    }

    public function saveParentNewRequestOthers($data){
            $parent_id = $data['userid'];
            $childid = $data['childid'];
            $action_totake = $data['actiontobetaken'];
            $reason = $data['reason'];
            $requester_relationship = 1;
            $request_updatebyid=0;
            $request_type= 1;
            $special_request_status ='Pending';

           $sqlchild = "SELECT par.child_id, par.centre_id,par.parent_name, cld.class_id, cld.class_level_id FROM tbl_parent par INNER JOIN tbl_child cld ON cld.child_id=par.child_id
             WHERE parent_id ='$parent_id'";
            $querychild = $this->db->query($sqlchild);
            if(! $querychild){
                return false;
            }else{
                $data = $querychild->row_array();
                $requester_name = $data['parent_name'];
                $centre_id = $data['centre_id'];
                $class_id = $data['class_id'];
                $childid= $data['child_id'];
                $class_level_id = $data['class_level_id'];

                $sql ="INSERT INTO tbl_child_special_request(`request_type`,`centre_id`,`class_id`,`class_level_id`,`parent_id`,`child_id`,`special_request_requester_name`,`special_request_relationship_id`,`special_request`,`special_request_remarks`,`special_request_status`,`special_request_updated_by_id`)
                VALUES('$request_type','$centre_id','$class_id','$class_level_id','$parent_id','$childid','$requester_name','$requester_relationship','$action_totake','$reason','$special_request_status','$request_updatebyid')";
                $query = $this->db->query($sql);
                if (!$query) {
                    return false;
                }else{
                    return true;
                }
            }
          
    }

    public function saveParentNewRequestMedication($data){
            $parent_id = $data['userid'];
            $childid = $data['childid'];
            $medicationintake = $data['medicationintake'];
            $medicationname = $data['medicationname'];
            $reason = $data['reason'];
            $requester_relationship = 1;
             $request_type= 2;
            $request_updatebyid=0;
            $special_request_status ='Pending';
            $createdate = date('Y-m-d h:i:s');

            $sqlchild = "SELECT par.child_id, par.centre_id,par.parent_name, cld.class_id, cld.class_level_id FROM tbl_parent par INNER JOIN tbl_child cld ON cld.child_id=par.child_id
             WHERE parent_id ='$parent_id'";
            $querychild = $this->db->query($sqlchild);
            if(!$querychild){
                return false;
            }else{
                $data = $querychild->row_array();
                $requester_name = $data['parent_name'];
                $centre_id = $data['centre_id'];
                $class_id = $data['class_id'];
                $childid= $data['child_id'];
                $class_level_id = $data['class_level_id'];


                $sql ="INSERT INTO tbl_child_special_request(`request_type`,`centre_id`,`class_id`,`class_level_id`,`parent_id`,`child_id`,`special_request_requester_name`,`special_request_relationship_id`,`special_request_remarks`,`medication_intake`,`medication_name`,`special_request_status`)
                VALUES('$request_type','$centre_id','$class_id','$class_level_id','$parent_id','$childid','$requester_name','$requester_relationship','$reason','$medicationintake','$medicationname','$special_request_status')";
                $query = $this->db->query($sql);
                if (!$query) {
                    return false;
                }else{
                    return true;
                }
                
                
            //    $sqlreq ="INSERT INTO tbl_administration_medication(`created_by_id`,`centre_id`,`class_id`,`class_level_id`,`child_id`,`requester_name`,`relation_with_child`,`request_status`,`medicine_administrated`,`is_repeat`,`interval_time`,`reason`,`created_date_time`)
            //     VALUES('0','$centre_id','$class_id','$class_level_id','$childid','$requester_name','$requester_relationship','$special_request_status','$medicationname','','$medicationintake','$reason','$createdate')";
            //     $queryreq = $this->db->query($sqlreq);
            //     $insert_id = $this->db->insert_id();
            //     if (!$queryreq) {
            //          return 'failure';
            //     }else{
            //          return true;
            //     }
                }
    }

    public function getParentRequestCount($data){
            $parent_id = $data['parentid'];
            $sqlcount = "SELECT sum( special_request_status = 'Pending') as pendingcount,
            sum( special_request_status = 'Accept') as acceptcount,
            sum( special_request_status = 'Reject') as rejecttcount,
            (select special_request_date_received from  tbl_child_special_request group by special_request_status = 'Pending') as pendingdate
             FROM tbl_child_special_request";

            $queryrequests = $this->db->query($sqlcount);
            $data = array();
            if($queryrequests){
                $data["otherrequests_list"] = $queryrequests->result_array();
                $data["total_requests"] = $queryrequests->num_rows();
                return $data;
            }else{
                return false;
            }
    }

    public function getParentRequestOthers($data){
            $parent_id = $data['parentid'];
            $childid = $data['childid'];
            $status = $data['pstatus'];
            $index = $data['index'];
            $count = $data['count'];
            $limit = ($index !=''&& $count!='') ? "desc LIMIT $index,$count" :(($index =='' && $count!='')? "desc LIMIT $count": ""  );
            $sqlrequests = "SELECT req.special_request_id, req.centre_id, req.c)lass_id, req.special_request_status, req.special_request_date_received, req.special_request, req.special_request_remarks, req.special_request_replied, req.special_request_reply_date, cnt.centre_name, cls.class_title 
             FROM `tbl_child_special_request` req INNER JOIN tbl_centre cnt ON cnt.centre_id=req.centre_id
             INNER JOIN tbl_class cls ON cls.class_id = req.class_id 
             WHERE child_id='$childid' and special_request_status='$status' and request_type='1' $limit";
            $queryrequests = $this->db->query($sqlrequests);
            $data = array();
            if($queryrequests){
                $data["otherrequests_list"] = $queryrequests->result_array();
                $data["total_requests"] = $queryrequests->num_rows();
                return $data;
            }else{
                return false;
            }
    }

    public function getParentRequestMedication($data){
            $parent_id = $data['parentid'];
            $childid = $data['childid'];
            $status = $data['pstatus'];
            $index = $data['index'];
            $count = $data['count'];
            $limit = ($index !=''&& $count!='') ? "desc LIMIT $index,$count" :(($index =='' && $count!='')? "desc LIMIT $count": ""  );
            
            $sqlrequests = "SELECT req.*, cnt.centre_name, cls.class_title 
             FROM `tbl_child_special_request` req INNER JOIN tbl_centre cnt ON cnt.centre_id=req.centre_id
             INNER JOIN tbl_class cls ON cls.class_id = req.class_id 
             WHERE req.child_id='$childid' and special_request_status='$status' and request_type='2' $limit";
            $queryrequests = $this->db->query($sqlrequests);
            $data = array();
            if($queryrequests){
                $data["otherrequests_list"] = $queryrequests->result_array();
                $data["total_requests"] = $queryrequests->num_rows();
                return $data;
            }else{
                return false;
            }
    }

    public function getParentChildTemperature($data) {
        $userid = $data['userid'];
        $childid = $data['childid'];
        $index = $data['index'];
        $count = $data['count'];
      $limit = ($index !=''&& $count!='') ? "desc LIMIT $index,$count" :(($index =='' && $count!='')? "desc LIMIT $count": ""  );
  
   $sql ="SELECT * FROM `tbl_child_attendance` WHERE child_id = '$childid'";
    //   $sql ="SELECT * FROM `tbl_child_health_records` WHERE child_id = '$childid'";
        $query = $this->db->query($sql);
        if($query){
        $data["temperature_list"] = $query->result_array();
        $data["total_records"] = $query->num_rows();
            return $data;
        }else{
         return false;
        }
    }

    public function getParentChildAttendance($data) {
        $userid = $data['userid'];
        $childid = $data['childid'];
        $index = $data['index'];
        $count = $data['count'];
      $limit = ($index !=''&& $count!='') ? "desc LIMIT $index,$count" :(($index =='' && $count!='')? "desc LIMIT $count": ""  );
  
        $sql ="SELECT * FROM `tbl_child_attendance` WHERE child_id = '$childid'  $limit";
        $query = $this->db->query($sql);
        if($query){
            $data["attendance_list"] = $query->result_array();
             $data["total_records"] = $query->num_rows();
            return $data;
        }else{
            return false;
        }
    }

    public function getParentChildNotifications($data=''){

    }
    
    public function getTermsandConditions($data=''){
        $sql ="SELECT id,terms FROM `tbl_app_content` where status='0'";
        $query = $this->db->query($sql);
        if($query){
            $data["termslist"] = $query->result_array();
            $data["total_records"] = $query->num_rows();
            return $data;
        }else{
            return false;
        }
    }
    public function getAboutContent($data=''){
        $sql ="SELECT id,about FROM `tbl_app_content` where status='0'";
        $query = $this->db->query($sql);
        if($query){
            $data["aboutdetail"] = $query->result_array();
            $data["total_records"] = $query->num_rows();
            return $data;
        }else{
            return false;
        }
    }
    
    public function getstaffTermsandConditions($data=''){
        $sql ="SELECT id,terms FROM `tbl_app_content` where status='1'";
        $query = $this->db->query($sql);
        if($query){
            $data["termslist"] = $query->result_array();
            $data["total_records"] = $query->num_rows();
            return $data;
        }else{
            return false;
        }
    }
    public function getstaffAboutContent($data=''){
        $sql ="SELECT id,about FROM `tbl_app_content` where status='1'";
        $query = $this->db->query($sql);
        if($query){
            $data["aboutdetail"] = $query->result_array();
            $data["total_records"] = $query->num_rows();
            return $data;
        }else{
            return false;
        }
    }
    

    public function SaveParentFeedback($data) {
        $feeduserid = $data['userid'];
        $feedtitle = $data['feedback_title'];
        $feeddetails=$data['feedback_details'];
        $sql = "INSERT INTO tbl_parent_feedback(`parent_id`,`feedback_title`,`feedback_details`) VALUES ('$feeduserid','$feedtitle','$feeddetails')";
        $query = $this->db->query($sql);
        if (!$query) {
            return 'failure';
        }else{
            return true;
        }
//        return $rset_get_staff[0];
//        return 1;
    }
    
    
    
    
    public function insertportfiles($imagename) {
        $sqlreq = "INSERT INTO tbl_portfolio_file(`file_img`,`file_posteddate`)VALUES('$imagename',now())";
        $queryreq = $this->db->query($sqlreq);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function createportfolio($data) {
        $userid = $data['userid'];
        
         $sqlcen = "select centre_id from tbl_staff where staff_id='$userid'";
            $querycen = $this->db->query($sqlcen);
            $sesscen = $querycen->result();
            foreach ($sesscen as $valcen) {
                $centreid = $valcen->centre_id;
            }
        
//        $centreid = $data['centreid'];
        $classid = $data['classid'];
        $childid = $data['childid'];
        $piesid = $data['piesid'];
        $title = $data['title'];
        $createdate = $data['date'];
        $crdate = date('Y-m-d h:i:s', strtotime($createdate));
        $venue = $data['venue'];
        $socials = $data['socials'];
        $cognitive = $data['cognitive'];

        $sqlreq = "INSERT INTO tbl_child_learning_portfolio(`centre_id`,`class_id`,`pies_id`,`title`,`venue`,`socials`,`cognitive`,`portfolio_updated_by_id`,`portfolio_created_date`,`portfolio_updated_date`)VALUES('$centreid','$classid','$piesid','$title','$venue','$socials','$cognitive','$userid','$crdate',now())";
        $queryreq = $this->db->query($sqlreq);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function uploadport($data, $portid, $fileid, $desc) {

        $userid = $data['userid'];
        $centreid = $data['centreid'];
        $classid = $data['classid'];
        $childid = $data['childid'];
        $title = $data['title'];
        $createdate = $data['date'];
        $crdate = date('Y-m-d h:i:s', strtotime($createdate));
        $venue = $data['venue'];
        $socials = $data['socials'];
        $cognitive = $data['cognitive'];

        $sqlreq = "INSERT INTO tbl_clp_selected_child(`portfolio_id`,`child_id`,`description`,`portfolio_files`,`create_date_time`)VALUES('$portid','$childid','$desc','$fileid',now())";
        $queryreq = $this->db->query($sqlreq);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
     public function SaveStaffFeedback($data) {
        $feeduserid = $data['userid'];
        $feedtitle = $data['feedback_title'];
        $feeddetails = $data['feedback_details'];
        if ($feeduserid != '' && $feedtitle != '') {
            $sql = "INSERT INTO tbl_parent_feedback(`parent_id`,`feedback_title`,`feedback_details`,`status`) VALUES ('$feeduserid','$feedtitle','$feeddetails','1')";
            $query = $this->db->query($sql);
            return true;
        }else{
            return false;
        }

    }
   
     public function insertuploadfiles($imagename, $data) {
        $staffid = $data['userid'];
        $mediatitle = $data['mediatitle'];
        $sqlreq = "INSERT INTO tbl_library(`staff_id`,`filetype`,`filename`,`folder_img_name`,`media_title`,`added_date`)VALUES('$staffid','Images','$imagename','$imagename','$mediatitle',now())";
        $queryreq = $this->db->query($sqlreq);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
     public function StaffCheckout($data) {
        $userid = $data['userid'];
        $studentid = $data['student_id'];
        $attendanceid = $data['attendance_id'];
        if ($userid != '' && $studentid != '' && $attendanceid != '') {
                $sql = "Update tbl_child_attendance SET child_sign_out_time=now()
				WHERE child_attendance_id = '" . $data["attendance_id"] . "' AND child_id = '" . $data["student_id"] . "'";
        $query = $this->db->query($sql);
            return true;
        } else {
            return false;
        }
    }
    
    public function stdsignin($data) {
        $userid = $data['userid'];
        $childid = $data['childid'];
        $centerid = $data['centre_id'];
        $classid = $data['class_id'];
        $curdate = date('Y-m-d');
        $curtime = date('h:i:s');

        $sqlchk = "select child_attendance_id from tbl_child_attendance where child_id = '$childid' and child_signin_date = '$curdate'";
        $querychk = $this->db->query($sqlchk);
        $numrows = $querychk->num_rows();



        if ($numrows == '0') {
            $sqlreq = "INSERT INTO tbl_child_attendance(`child_id`,`centre_id`,`class_id`,`child_signin_date`,`child_sign_in_time`,`staff_id`)VALUES('$childid','$centerid','$classid',now(),now(),'$userid')";
            $queryreq = $this->db->query($sqlreq);
            $insert_id = $this->db->insert_id();
        } else {
            $sessrole1 = $querychk->result();
            foreach ($sessrole1 as $val1) {
                $attendanceid = $val1->child_attendance_id;
                if ($attendanceid != '') {
                    $attendanceid;
                } else {
                    $attendanceid = "";
                }
            }
            $sqlreq = "UPDATE tbl_child_attendance SET child_signin_date=now(),staff_id='$userid',child_sign_in_time=now(),child_sign_out_time='00:00:00' WHERE child_attendance_id='$attendanceid'";
           $queryreq = $this->db->query($sqlreq);
            $insert_id = $attendanceid;
        }


        return $insert_id;
    }
     public function getChildnames($data) {
        $sqlchilds = ("SELECT child_name FROM `tbl_child` where child_id IN ($data)");
        $query = $this->db->query($sqlchilds);

        $data = array();
        $data["child_lists"] = $query->result_array();
        $data["total_childs"] = $query->num_rows();
        return $data;
    }
    
    public function GetallStaff($data) {
        $start = $data['index'];
        $limit = $data['count'];
        if ($start == '' && $limit == '') {
            $sql = "SELECT * FROM tbl_staff WHERE visible_status = '1'";
        } else if ($start == '' && $limit != '') {
            $sql = "SELECT * FROM tbl_staff WHERE visible_status = '1' LIMIT $limit";
        } else if ($start != '' && $limit != '') {
            $sql = "SELECT * FROM tbl_staff WHERE visible_status = '1' LIMIT $start,$limit";
        }
        $query = $this->db->query($sql);

        $data = array();
        $data["staff_list"] = $query->result_array();
        $data["total_staff"] = $query->num_rows();
        return $data;
    }

}

?>