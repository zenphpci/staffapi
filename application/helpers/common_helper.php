<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
function createthumb($name,$filename,$new_w,$new_h)
{
		/*$system=strtolower(end(explode(".",$name)));
		if (preg_match("/jpg|jpeg/",$system)){$src_img=imagecreatefromjpeg($name);}
		if (preg_match("/png/",$system)){$src_img=imagecreatefrompng($name);}
		if (preg_match("/gif/",$system)){$src_img=imagecreatefromgif($name);}*/
		$name = strtolower($name);
		$system=explode(".",$name);
		$cnt = count($system);
		if($cnt>0){
			$ext = strtolower($system[$cnt-1]);
		}else{
			$ext = "";
		}
		$src_img = "";			
		
		if (preg_match("/jpg|jpeg/",$system[1])){
			$src_img=imagecreatefromjpeg($name);				
		}else if (preg_match("/png/",$system[1])){
			$src_img=imagecreatefrompng($name);				
		}else if (preg_match("/gif/",$system[1])){
			$src_img=imagecreatefromgif($name);				
		}else if (preg_match("/bmp/",$system[1])){
			$src_img=imagecreatefromwbmp($name);				
		}else if($ext=="jpg" || $ext=="jpeg" || $ext=="JPEG" || $ext=="JPG"){
			$src_img=imagecreatefromjpeg($name);
		}else if($ext=="gif" || $ext=="GIF"){
			$src_img=imagecreatefromgif($name);
		}else if($ext=="png" || $ext=="PNG"){
			$src_img=imagecreatefrompng($name);
		}else if($ext=="bmp" || $ext=="BMP"){
			$src_img=ImageCreateFromBMP($name);
		}else{
			$src_img=imagecreatefromjpeg($name);
		}
		
		
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		
		if ($old_x > $old_y) 
		{
			$thumb_w=$new_w;
			$thumb_h=$old_y*($new_h/$old_x);
		}
		if ($old_x < $old_y) 
		{
			$thumb_w=$old_x*($new_w/$old_y);
			$thumb_h=$new_h;
		}
		if ($old_x == $old_y) 
		{
			$thumb_w=$new_w;
			$thumb_h=$new_h;
		}
				
		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
		if (preg_match("/png/",$system[1]))
		{
			imagepng($dst_img,$filename); 
		}
		else 
		{
			imagejpeg($dst_img,$filename); 
		}
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	
}
