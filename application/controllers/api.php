<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->db = $this->load->database('default', true);
        $this->load->helper('common_helper');
        $this->load->model('login_model');
    }

    public function Staff_Login() {
        $datas = json_decode(file_get_contents("php://input"), true);

        $email = $datas['username'];
        $pwd = md5($datas['password']);
        $devicetype = $datas['devicetype'];
        $devicetoken = $datas['devicetoken'];
// login wizard post data
        $staffarray = array('email' => $email, 'password' => $pwd);
        $devicearray = array('devicetype' => $devicetype, 'devicetoken' => $devicetoken, 'email' => $email);

        $invalidLogin = ['invalid' => $email];
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getStaff($arr_staff_login_post_data);
            header('Content-Type: application/json; charset=utf-8');
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

//                $sqlmsin = "Update tbl_staff set staff_login_status='3',devicetype='$devicetype',devicetoken='$devicetoken' where staff_login_id = '$email'";
//                $login_staffss = $this->db->query($sqlmsin);

                $updatedata = $this->login_model->updateStaff($devicearray);
                $loiginids_data = $this->login_model->getStaff($arr_staff_login_post_data);
                $response = '1';

                $arr_staff["userid"] = $arr_staff_login_data["staff_id"];
                $arr_staff["emailid"] = $email;
                $arr_staff["staff_quickbloxid"] = $arr_staff_login_data["quickblox_id"];
                 $arr_staff["staffname"] = $arr_staff_login_data["staff_name"];
                $arr_staff["roleid"] = $arr_staff_login_data["staff_job_role_id"];
                $arr_staff["staffmode"] = $arr_staff_login_data["staff_mode"];
                $arr_staff["centerid"] = $arr_staff_login_data["centre_id"];
                $arr_staff["centername"] = $arr_staff_login_data["centre_name"];
                $arr_staff["login_status"] = $loiginids_data["staff_login_status"];
                $arr_staff["photourl"] = VIEW_DIR . 'staff/thumb/' . $arr_staff_login_data["staff_photo"];
                $sess_staff = array("response" => '1', "login_datas" => $arr_staff);
                echo json_encode($sess_staff);
                die();
            } else {
                $sess_staff = array("response" => '0', "login_datas" => '');
                echo json_encode($sess_staff);
                die;
            }
        }
    }
    public function Parent_Login() {
        $datas = json_decode(file_get_contents("php://input"), true);

        $email = $datas['username'];
        $pwd = md5($datas['password']);
        $devicetype = $datas['devicetype'];
        $devicetoken = $datas['devicetoken'];
// login wizard post data
        $staffarray = array('email' => $email, 'password' => $pwd);
        $devicearray = array('devicetype' => $devicetype, 'devicetoken' => $devicetoken, 'email' => $email);

        $invalidLogin = ['invalid' => $email];
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getParent($arr_staff_login_post_data);
            header('Content-Type: application/json; charset=utf-8');
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

//                $sqlmsin = "Update tbl_staff set staff_login_status='3',devicetype='$devicetype',devicetoken='$devicetoken' where staff_login_id = '$email'";
//                $login_staffss = $this->db->query($sqlmsin);

                $updatedata = $this->login_model->updateParent($devicearray);
                $loiginids_data = $this->login_model->getParent($arr_staff_login_post_data);
                $response = '1';
//                $arr_staff["response"] = '1';

                $photourl = $arr_staff_login_data["photourl"];

               if($photourl != ''){
                $pics = VIEW_DIR . 'parent/thumb/' . $photourl;
            }else{
                $pics = "";
                }


   $classid = $arr_staff_login_data["class_id"];

                $sqljob = "select class_title from tbl_class where class_id='$classid'";
                $queryjb = $this->db->query($sqljob);
                $sessrole = $queryjb->result();
                foreach ($sessrole as $val) {
                    $class_name = $val->class_title;
                }

                $arr_staff["userid"] = $arr_staff_login_data["parent_id"];
                $arr_staff["quickbloxid"] = $arr_staff_login_data["quickblox_id"];
                $arr_staff["emailid"] = $email;
                $arr_staff["parentname"] = $arr_staff_login_data["parent_name"];
                $arr_staff["childid"] = $arr_staff_login_data["child_id"];
                $arr_staff["chindname"] = $arr_staff_login_data["child_name"];
                $arr_staff["centerid"] = $arr_staff_login_data["centre_id"];
                $arr_staff["centername"] = $arr_staff_login_data["centre_name"];
                 $arr_staff["classid"] = $arr_staff_login_data["class_id"];
                $arr_staff["classname"] = $class_name;
                $arr_staff["login_status"] = $loiginids_data["parent_login_status"];
                $arr_staff["photourl"] = $pics;
                $sess_staff = array("response" => '1', "login_datas" => $arr_staff);
                echo json_encode($sess_staff);
                die();
            } else {
$sess_staff = array("response" => '0', "login_datas" => '');
                echo json_encode($sess_staff);
                die;
            }
        }
    }

    public function Staff_Logout() {

        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
//        $staffid = base64_decode($staffid);
        $staffarray = array('staffid' => $staffid);
        $arr_staff_login_post_data = $staffarray;
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getStaffbyid($arr_staff_login_post_data);
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

                $sql = "Update tbl_staff set staff_login_status='2' where staff_id = '$staffid'";
                $login_staff = $this->db->query($sql);

                $arr_logindatas = $this->login_model->getStaffbyid($arr_staff_login_post_data);

                $arr_staff["login_status"] = $arr_logindatas["staff_login_status"];
                $sess_staff = array("response" => '1', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            } else {
                $arr_staff["responsemsg"] = 'User id does not exists';
                $sess_staff = array("response" => '0', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
            }
        } else {
            $arr_staff["responsemsg"] = 'User id cannot be empty';
            $sess_staff = array("response" => '0', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
            echo json_encode($sess_staff);
        }
//        print_r($arr_staff_login_post_data);
    }
    public function Parent_Logout() {

        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
//        $staffid = base64_decode($staffid);
        $staffarray = array('staffid' => $staffid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getParentbyid($arr_staff_login_post_data);
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

                $sql = "Update tbl_parent set parent_login_status='2' where parent_id = '$staffid'";
                $login_staff = $this->db->query($sql);

                $arr_logindatas = $this->login_model->getParentbyid($arr_staff_login_post_data);

                $arr_staff["login_status"] = $arr_logindatas["parent_login_status"];
                $sess_staff = array("response" => '1', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            } else {
                $arr_staff["responsemsg"] = 'User id does not exists';
                $sess_staff = array("response" => '0', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
            }
        } else {
            $arr_staff["responsemsg"] = 'User id cannot be empty';
            $sess_staff = array("response" => '0', "logout_details" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
            echo json_encode($sess_staff);
        }
//        print_r($arr_staff_login_post_data);
    }

    public function Staff_ForgotPasssword() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffmail = $datas['emailid'];
//        $staffid = base64_decode($staffid);
        $staffarray = array('staffmail' => $staffmail);
        $arr_staff_login_post_data = $staffarray;
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getStaffbyemailid($arr_staff_login_post_data);
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

                $arr_staff["msg"] = 'please check your mail to get back your password';
                $sess_staff = array("response" => '1', "forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            } else {
                $arr_staff["response"] = '0';
                $arr_staff["msg"] = 'Mail Id is not yet registered';
                $sess_staff = array("forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            }
        } else {
            $arr_staff["response"] = '0';
            $arr_staff["msg"] = 'Enter Mail id';
            $sess_staff = array("forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
            echo json_encode($sess_staff);
            die();
        }
//        print_r($arr_staff_login_post_data);
    }
    public function Parent_ForgotPasssword() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffmail = $datas['emailid'];
//        $staffid = base64_decode($staffid);
        $staffarray = array('staffmail' => $staffmail);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_login_data = $this->login_model->getParentbyemailid($arr_staff_login_post_data);
            if (!empty($arr_staff_login_data) && count($arr_staff_login_data) > 0) {

                $arr_staff["msg"] = 'please check your mail to get back your password';
                $sess_staff = array("response" => '1', "forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            } else {
                $arr_staff["response"] = '0';
                $arr_staff["msg"] = 'Mail Id is not yet registered';
                $sess_staff = array("forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
                echo json_encode($sess_staff);
                die();
            }
        } else {
            $arr_staff["response"] = '0';
            $arr_staff["msg"] = 'Enter Mail id';
            $sess_staff = array("forgot_pwd" => $arr_staff);
//                print_r($sess_staff);
//                $this->session->set_userdata($sess_staff);
            echo json_encode($sess_staff);
            die();
        }
//        print_r($arr_staff_login_post_data);
    }

    public function Staff_GetAllCenters() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
 header('Content-Type: application/json; charset=utf-8');
        $staffarray = array('staffid' => $staffid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
             $sqljob = "select quickblox_id from tbl_staff where staff_id='$staffid'";
                $queryjb = $this->db->query($sqljob);
                $sessrole = $queryjb->result();
                foreach ($sessrole as $val) {
                    $quickbloxid = $val->quickblox_id;
                }
            $arr_staff_centre = $this->login_model->getallcenters($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['centres'] = array();
            $totalrows = $arr_staff_centre['total_centre'];
            if ($totalrows > 0) {
                foreach ($arr_staff_centre["centre_list"] as $view_staff_centre) {
                    extract($view_staff_centre);
                    $rows = array();
                    $rows['centreid'] = $centre_id;
                    $rows['centrename'] = $centre_name;
                  //  $rows['quickbloxid'] = $quickbloxid;


                    array_push($response['centres'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $response['response'] = '0';
                
             
                array_push($response['centres']);
            }
            echo json_encode($response);
        }
    }

    public function Staff_GetAllClasses() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $centreid = $datas['centreid'];

        $staffarray = array('staffid' => $staffid, 'centreid' => $centreid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_classes = $this->login_model->getallclasses($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['classes'] = array();
            $totalrows = $arr_staff_classes['total_classes'];
            if ($totalrows > 0) {
                 
                foreach ($arr_staff_classes["classes_list"] as $view_staff_class) {
//                    print_r($view_staff_class);
                    extract($view_staff_class);
                    $rows = array();
                   
                    $rows['classid'] = $class_id;
                    $rows['classname'] = $class_title;
                    

                    array_push($response['classes'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $response['response'] = '0';
                $rows = array();
                $rows['classid'] = "";
                $rows['classname'] = "";

                array_push($response['classes'], $rows);
            }
            echo json_encode($response);
        }
    }

    public function Staff_GetAllChildren() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $classid = $datas['classid'];
 header('Content-Type: application/json; charset=utf-8');
        $staffarray = array('staffid' => $staffid, 'classid' => $classid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_child = $this->login_model->getallchildrens($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['childrens'] = array();
            $totalrows = $arr_staff_child['total_childrens'];
            
            $sqljob = "select quickblox_id from tbl_staff where staff_id='$staffid'";
        $queryjb = $this->db->query($sqljob);
        
        $sessrole = $queryjb->result();
        foreach ($sessrole as $val) {
            $quickblox_id = $val->quickblox_id;
        }
        
        
        
            
            if ($totalrows > 0) {
                foreach ($arr_staff_child["child_list"] as $view_staff_child) {
//                    print_r($view_staff_class);
                    extract($view_staff_child);
                    
                     $sqlchld = "select parent_name from tbl_parent where child_id='$child_id'";
            $querychld = $this->db->query($sqlchld);
            if($querychld->num_rows()){
            $sesschld = $querychld->result();
            foreach ($sesschld as $valss) {
                $parentname = $valss->parent_name;
            }
            }else{
                $parentname = "";
            }
                    
                    $rows = array();
                    $rows['childid'] = $child_id;
                    $rows['childname'] = $child_name;
                    $rows['parentname'] = $parentname;
                      $rows['childphoto'] = CHILD_WEB . 'thumb/' . $child_photo;
                 //   $rows['quickbloxid'] = $quickblox_id;

                    array_push($response['childrens'], $rows);
                    $parentname= "";
//                print_r($view_portfolio_list);
                }
            } else {
                $response['response'] = '0';
                $rows = array();
                $rows['childid'] = "";
                $rows['childname'] = "";

                array_push($response['childrens'], $rows);
            }
            echo json_encode($response);
        }
    }

    public function Staff_AnnouncementCategory() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];

        $staffarray = array('staffid' => $staffid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_child = $this->login_model->getaannouncemetcat($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['announcement_category'] = array();
            $totalrows = $arr_staff_child['total_category'];
            if ($totalrows > 0) {
                foreach ($arr_staff_child["category_list"] as $view_category) {
//                    print_r($view_category);
                    extract($view_category);
                    $rows = array();
                    $rows['catid'] = $event_catid;
                    $rows['catname'] = $event_title;

                    array_push($response['announcement_category'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $rows = array();
                $rows['catid'] = "";
                $rows['catname'] = "";

                array_push($response['announcement_category'], $rows);
            }
            echo json_encode($response);
        }
    }

    public function staffportfolio($staffid) {
        $staffid = base64_decode($staffid);

        $staffarray = array('staffid' => $staffid);
        $arr_staff_login_post_data = $staffarray;
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_portofolio = $this->login_model->getStaffportfolio($arr_staff_login_post_data);
            $response['portfolio'] = array();
            $totalrows = $arr_staff_portofolio['total_portfolio'];
            if ($totalrows > 0) {
                foreach ($arr_staff_portofolio["portfolio_list"] as $view_portfolio_list) {
                    extract($view_portfolio_list);
                    $rows = array();
                    $rows['portid'] = $portfolio_id;
                    $rows['porttitle'] = $title;
                    $rows['piesname'] = $pies_name;
                    $rows['classname'] = $class_level_name;
                    $rows['created_date'] = date('d-M-Y', strtotime($portfolio_created_date));

                    array_push($response['portfolio'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $rows = array();
                $rows['portid'] = "";
                $rows['porttitle'] = "";
                $rows['piesname'] = "";
                $rows['classname'] = "";
                $rows['created_date'] = "";

                array_push($response['portfolio'], $rows);
            }
            echo json_encode($response);
        }
    }

    

    public function Staff_GetSpecialRequests($id = '') {
        if ($id == "") {
            $datas = json_decode(file_get_contents("php://input"), true);
            $staffid = $datas['userid'];
            $index = $datas['index'];
            $limit = $datas['count'];
            $staffarray = array('staffid' => $staffid, 'index' => $index, 'count' => $limit);
            $arr_staff_login_post_data = $staffarray;
            header('Content-Type: application/json; charset=utf-8');
            if (!empty($arr_staff_login_post_data)) {
                $arr_staff_requests = $this->login_model->getStaffrequests($arr_staff_login_post_data);
               // print_r($arr_staff_requests);
                $response['response'] = '1';
                $response['special_requests'] = array();

                $totalrows = $arr_staff_requests['total_requests'];
                if ($totalrows > 0) {
                    foreach ($arr_staff_requests["requests_list"] as $view_requests_list) {
                        extract($view_requests_list);
//                        print_r($view_requests_list);
                        $replydate = $special_request_reply_date;
                        if ($special_request_replied == '') {
                            $finalreplydate = '';
                        } else {
                            $createddate1 = date('Y-M-d', strtotime($replydate));
                            $showdate1 = date('d F Y', strtotime($replydate));
                            $createdtime1 = date('g:i A', strtotime($replydate));
                            $nameOfDay1 = date('l', strtotime($createddate1));
                            $finalreplydate = $nameOfDay1 . ',' . $showdate1 . ' ' . $createdtime1;
                        }

                        $createddate = date('Y-M-d', strtotime($special_request_date_received));
                        $showdate = date('d F Y', strtotime($special_request_date_received));
                        $createdtime = date('g:i A', strtotime($special_request_date_received));
                        $nameOfDay = date('l', strtotime($createddate));

                       if($parent_id == '0'){
                       $parent_name = "";
                       } 
 if($special_request_status == 'Accept'){
            $special_request_status = 'Accepted';
        }else if($special_request_status == 'Reject'){
            $special_request_status = 'Rejected';
        }else{
            $special_request_status;
        }
        
        if($special_request_replied == ''){
                            $replied = '0';
                        }else{
                           $replied = '1'; 
                        }
        
                        $rows = array();
                        $rows['requestid'] = $special_request_id;
                        $rows['requesttype'] = $request_type;
                        $rows['parentid'] = $parent_id;
                        $rows['parentname'] = $parent_name;
                        $rows['from_name'] = $special_request_requester_name;
                        $rows['childid'] = $child_id;
                        $rows['childname'] = $child_name;
                        $rows['photourl'] = CHILD_THUMB_WEB.$child_photo;
                        $rows['centerid'] = $centre_id;
                        $rows['centername'] = $centre_name;
                        $rows['actiontobetaken'] = $special_request;
                        $rows['reason'] = $special_request_remarks;
                          $rows['classid'] = $class_id;
                        $rows['classname'] = $class_title;
//                        $rows['class'] = $class_title;
                        $rows['dateandtime'] = $nameOfDay . ',' . $showdate . ' ' . $createdtime;
                        $rows['status'] = $special_request_status;
                        $rows['meditationintake'] = $medication_intake;
                        $rows['meditationname'] = $medication_name;
                        $rows['replied'] = $replied;
                        $rows['reply'] = $special_request_replied;
                        $rows['replydateandtime'] = $finalreplydate;

                        array_push($response['special_requests'], $rows);
                    }
                } else {
                    array_push($response['special_requests'], $rows);
                }
                echo json_encode($response);
            }
        } else {
            $requestid = $id;
            $requestarray = array('request_id' => $requestid);
            $arr_request_post_data = $requestarray;
            if (!empty($arr_request_post_data)) {
                $arr_requests_details = $this->login_model->getrequestsbyid($requestid);
                $response['special_requests'] = array();
                $totalrows = $arr_requests_details['total_requests'];
//                echo $totalrows.'svasams';
//                die();
                if ($totalrows > 0) {
                    foreach ($arr_requests_details["requests_details"] as $view_requests_id) {
                        extract($view_requests_id);
//                    print_r($view_requests_id);
                        $rows = array();
//                        $response['response'] = '1';
                        $rows['requestid'] = $special_request_id;
                        $rows['from_name'] = $special_request_requester_name;
                        $rows['child_name'] = $child_name;
                        $rows['centre_name'] = $centre_name;
                        $rows['class'] = $class_title;
                        $rows['created_date'] = date('d-M-Y', strtotime($special_request_date_received));

                        array_push($response['special_requests'], $rows);
//                print_r($view_portfolio_list);
                    }
                } else {
                    $rows = array();
                    $rows['requestid'] = '';
                    $rows['from_name'] = '';
                    $rows['child_name'] = '';
                    $rows['centre_name'] = '';
                    $rows['class'] = '';
                    $rows['created_date'] = '';

                    array_push($response['special_requests'], $rows);
                }
                echo json_encode($response);
            }
        }
    }

    public function Staff_GetAllUploadedFiles() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $index = $datas['index'];
        $limit = $datas['count'];
        $staffarray = array('staffid' => $staffid, 'index' => $index, 'count' => $limit);
        $arr_staff_login_post_data = $staffarray;
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_requests = $this->login_model->getStaffuploadfiles($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['libraries'] = array();

            $totalrows = $arr_staff_requests['total_files'];
            if ($totalrows > 0) {
                foreach ($arr_staff_requests["file_list"] as $view_requests_list) {
                    extract($view_requests_list);
                    $createddate = date('Y-M-d', strtotime($added_date));
                    $showdate = date('d M Y', strtotime($added_date));
                    $nameOfDay = date('l', strtotime($added_date));
                    $finaldate = $nameOfDay . ',' . $showdate;
                    $rows = array();
                    $rows['fileid'] = $id;
                    $rows['mediatitle'] = $media_title;
                    $rows['fileurl'] = VIEW_DIR . 'library/' . $folder_img_name;
                    $rows['filetype'] = $filetype;
                    $rows['createdtime'] = $finaldate;

                    array_push($response['libraries'], $rows);
                }
            } else {
               

                array_push($response['libraries']);
            }
            echo json_encode($response);
        }
    }

    public function verifyrequests() {
        $requestid = $_POST['requestid'];
        $requestarray = array('request_id' => $requestid);
        $arr_request_post_data = $requestarray;
        if (!empty($arr_request_post_data)) {
            $arr_requests_details = $this->login_model->getrequestsbyid($requestid);
            print_r($arr_requests_details);
            die();

            $response['special_requests'] = array();
            $totalrows = $arr_staff_requests['total_requests'];
            if ($totalrows > 0) {
//                foreach ($arr_staff_requests["requests_list"] as $view_requests_list) {
                extract($view_requests_list);
//                    print_r($view_requests_list);
                $rows = array();
                $rows['requestid'] = $special_request_id;
                $rows['from_name'] = $special_request_requester_name;
                $rows['child_name'] = $child_name;
                $rows['centre_name'] = $centre_name;
                $rows['class'] = $class_title;
                $rows['created_date'] = date('d-M-Y', strtotime($special_request_date_received));

                array_push($response['special_requests'], $rows);
//                print_r($view_portfolio_list);
//                }
            } else {
                $rows = array();
                $rows['portid'] = "";
                $rows['porttitle'] = "";
                $rows['piesname'] = "";
                $rows['classname'] = "";
                $rows['created_date'] = "";

                array_push($response['portfolio'], $rows);
            }
            echo json_encode($response);
        }
    }

    public function Parent_CreateNewRequest_Others(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        $childid = $data['childid'];
        $actiontotake = $data['actiontobetaken'];
        $reason = $data['reason'];
        header('Content-Type: application/json; charset=utf-8');
        if( $parentid !='' && $actiontotake!='' && $reason !=''){


            $parentRequestarray = array('userid' => $parentid, 'childid'=>$childid, 'actiontobetaken' => $actiontotake, 'reason' => $reason);
            $parent_Newrequestothers_ins = $this->login_model->saveParentNewRequestOthers($parentRequestarray);
            $arr_parent = array();
            if ($parent_Newrequestothers_ins) { 
            $arr_parent["msg"] = 'Your request has been submitted to the centre successfully.';
            $sess_parent = array("response" => '1', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
            }else{
                $arr_parent["msg"] = 'New Request not saved';
                $sess_parent = array("response" => '0', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
            }
        }else{
                $arr_parent['msg'] = 'Please fill in the Action to be Taken and Reason in order to submit.`';
                $sess_parent = array("response" => '0', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
        }
    }

    public function Parent_CreateNewRequest_Medications(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        $childid = $data['childid'];
        $medicineintake = $data['medicationintake'];
        $medicationname = $data['medicationname'];
        $reason = $data['reason'];
        header('Content-Type: application/json; charset=utf-8');
        if( $parentid !='' && $medicineintake!='' && $medicationname !=''){


            $parentRequestarray = array('userid' => $parentid, 'childid'=>$childid, 'medicationintake' => $medicineintake,'medicationname'=>$medicationname, 'reason' => $reason);
            $parent_Newrequestothers_ins = $this->login_model->saveParentNewRequestMedication($parentRequestarray);
            $arr_parent = array();
            if ($parent_Newrequestothers_ins) { 
            $arr_parent["msg"] = 'Your request has been submitted to the centre successfully.';
            $sess_parent = array("response" => '1', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
            }else{
                $arr_parent["msg"] = 'New Request not saved';
                $sess_parent = array("response" => '0', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
            }
        }else{
                $arr_parent['msg'] = 'Please fill in the Action to be Taken and Reason in order to submit.`';
                $sess_parent = array("response" => '0', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
        }
    }

     public function Parent_GetRequestCount() {
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
      //  header('Content-Type: application/json; charset=utf-8');
        if (!empty($parentid)) {
            $arr_parentrequest_others = $this->login_model->getParentRequestCount($parentid);
 $date_rec = "";$date_rec1 = "";$date_rec2="";
            $sqljob = "select special_request_date_received from tbl_child_special_request where special_request_status='Pending' and parent_id='$parentid' ORDER BY special_request_id DESC LIMIT 1";
            $queryjb = $this->db->query($sqljob);
            if($queryjb){
            $sessrole = $queryjb->result();
            foreach ($sessrole as $val) {
                $pendate = $val->special_request_date_received;
                if ($pendate != '') {
                    $date_rec = date('d M Y', strtotime($pendate));
                } else {
                    $date_rec = "";
                }
            }
            }else{
                 $date_rec = "";
            }
            $sqljob1 = "select special_request_date_received from tbl_child_special_request where special_request_status='Accept' and parent_id='$parentid' ORDER BY special_request_id DESC LIMIT 1";
            $queryjb1 = $this->db->query($sqljob1);
            if($queryjb1){
            $sessrole1 = $queryjb1->result();
            foreach ($sessrole1 as $val1) {
                $accdate = $val1->special_request_date_received;
                if ($accdate != '') {
                    $date_rec1 = date('d M Y', strtotime($accdate));
                } else {
                    $date_rec1 = "";
                }
            }
            }else{
                 $date_rec1 = "";
            }
           $sqljob2 = "select special_request_date_received from tbl_child_special_request where special_request_status='Reject' and parent_id='$parentid' ORDER BY special_request_id DESC LIMIT 1";
            $queryjb2 = $this->db->query($sqljob2);
            if($queryjb2){
            $sessrole2 = $queryjb2->result();
            foreach ($sessrole2 as $val2) {
                $rejdate = $val2->special_request_date_received;
                if ($rejdate != '') {
                    $date_rec2 = date('d M Y', strtotime($rejdate));
                } else {
                    $date_rec2 = "";
                }
            }
            }else{
                $date_rec2="";
            }
            $response['response'] = '1';
            $response['request_others_response'] = array();
            $totalrows = $arr_parentrequest_others['total_requests'];
            if ($totalrows > 0) {
                foreach ($arr_parentrequest_others["otherrequests_list"] as $view_reqlists) {
//                    print_r($view_category);
                    extract($view_reqlists);
                    $rows = array();
                    $rows['pendingcount'] = $pendingcount;
                    $rows['acceptcount'] = $acceptcount;
                    $rows['rejecttcount'] = $rejecttcount;
                    $rows['pendinglastupdateddate'] = $date_rec;
                    $rows['acceptcountupdateddate'] = $date_rec1;
                    $rows['rejecttcountupdateddate'] = $date_rec2;
                    array_push($response['request_others_response'], $rows);
                }
            } else {
                array_push($response['request_others_response']);
            }
            echo json_encode($response);
        }
    }

    public function Parent_GetOthersRequests() {
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        $childid = $data['childid'];
        $reqstatus  = $data['status'];
        $index  = $data['index'];
        $count  = $data['count'];    
        
          if($reqstatus == 'Accepted'){
            $reqstatus = 'Accept';
        }else if($reqstatus == 'Rejected'){
            $reqstatus = 'Reject';
        }else{
            $reqstatus;
        }
        
header('Content-Type: application/json; charset=utf-8');
        $requestsarray = array('parentid' => $parentid,'childid'=>$childid,'pstatus'=>$reqstatus,'index'=>$index,'count'=>$count);
        if (!empty($requestsarray)) {
         
            $arr_parentrequest_others = $this->login_model->getParentRequestOthers($requestsarray);
            $response['response'] = '1';
            $response['request_others_response'] = array();
            $totalrows = $arr_parentrequest_others['total_requests'];
            if ($totalrows > 0) {
                foreach ($arr_parentrequest_others["otherrequests_list"] as $view_reqlists) {
//                    print_r($view_category);
                    extract($view_reqlists);
                    $rows = array();

                        $replydate = $special_request_reply_date;
                        if ($replydate == '0000-00-00 00:00:00') {
                            $finalreplydate = '';
                        } else {
                            $createddate1 = date('Y-M-d', strtotime($replydate));
                            $showdate1 = date('d F Y', strtotime($replydate));
                            $createdtime1 = date('g:i A', strtotime($replydate));
                            $nameOfDay1 = date('l', strtotime($createddate1));
                            $finalreplydate = $nameOfDay1 . ',' . $showdate1 ;
                        }
                        if ($special_request_date_received == '0000-00-00 00:00:00') {
                             $nameOfDay = '';
                             $showdate = '';
                        } else {
                             $createddate = date('Y-M-d', strtotime($special_request_date_received));
                             $showdate = date('d F Y', strtotime($special_request_date_received));
                             $createdtime = date('g:i A', strtotime($special_request_date_received));
                             $nameOfDay = date('l', strtotime($createddate));
                        }
                        
                           if($special_request_replied == ''){
                            $replied = '0';
                        }else{
                           $replied = '1'; 
                        }
                        
                        
                      if ($special_request_status == 'Accept'){
                          $special_request_status = 'Accepted';
                      } else if ($special_request_status == 'Reject'){
                          $special_request_status = 'Rejected';
                      }else{
                          $special_request_status = 'Pending';
                      }
                        
                    $rows['requestid'] = $special_request_id;
                    $rows['centerid'] = $centre_id;
                    $rows['centername'] = $centre_name;
                    $rows['datesubmitted'] = $nameOfDay .' '. $showdate;
                    $rows['actiontobetaken'] = $special_request;
                    $rows['reason'] = $special_request_remarks;
                    $rows['classid'] = $class_id;
                    $rows['classname'] = $class_title;
                    $rows['centerreply'] = $special_request_replied;
                    $rows['replydate'] = $finalreplydate;
                    $rows['status'] = $special_request_status;
                    $rows['replied'] = $replied;
                    

                    array_push($response['request_others_response'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
               
                array_push($response['request_others_response']);
            }
            echo json_encode($response);
        }
    }

    public function Parent_GetMedicationrequests() {
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        $childid = $data['childid'];
        $reqstatus  = $data['status'];
        $index  = $data['index'];
        $count  = $data['count'];
        
        if($reqstatus == 'Accepted'){
            $reqstatus = 'Accept';
        }else if($reqstatus == 'Rejected'){
            $reqstatus = 'Reject';
        }else{
            $reqstatus;
        }

        $requestsarray = array('parentid' => $parentid,'childid'=>$childid,'pstatus'=>$reqstatus,'index'=>$index,'count'=>$count);
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($requestsarray)) {
            $arr_parentrequest_others = $this->login_model->getParentRequestMedication($requestsarray);
          
            $response['response'] = '1';
            $response['request_others_response'] = array();
            $totalrows = $arr_parentrequest_others['total_requests'];
            if ($totalrows > 0) {
                foreach ($arr_parentrequest_others["otherrequests_list"] as $view_reqlists) {
//                    print_r($view_category);
                    extract($view_reqlists);
                    $rows = array();

                        $replydate = $special_request_reply_date;
                        if ($replydate == '0000-00-00 00:00:00') {
                            $finalreplydate = '';
                        } else {
                            $createddate1 = date('Y-M-d', strtotime($replydate));
                            $showdate1 = date('d F Y', strtotime($replydate));
                            $createdtime1 = date('g:i A', strtotime($replydate));
                            $nameOfDay1 = date('l', strtotime($createddate1));
                            $finalreplydate = $nameOfDay1 . ',' . $showdate1 ;
                        }
                        if ($created_date_time == '0000-00-00 00:00:00') {
                             $nameOfDay = '';
                             $showdate = '';
                        } else {
                             $createddate = date('Y-M-d', strtotime($special_request_date_received));
                             $showdate = date('d F Y', strtotime($special_request_date_received));
                             $createdtime = date('g:i A', strtotime($special_request_date_received));
                             $nameOfDay = date('l', strtotime($special_request_date_received));
                        }
                        
                        
                        if($special_request_replied == ''){
                            $replied = '0';
                        }else{
                           $replied = '1'; 
                        }
                        
                        
                      if ($special_request_status == 'Accept'){
                          $special_request_status = 'Accepted';
                      } else if ($special_request_status == 'Reject'){
                          $special_request_status = 'Rejected';
                      }else{
                          $special_request_status = 'Pending';
                      }
                    $rows['requestid'] = $special_request_id;
                    $rows['centerid'] = $centre_id;
                    $rows['centername'] = $centre_name;
                    $rows['datesubmitted'] = $nameOfDay .' '. $showdate;
                    $rows['medicationname'] = $medication_name;
                     $rows['medicationintake'] = $medication_intake;
                    $rows['reason'] = $special_request_remarks;
                    $rows['classid'] = $class_id;
                    $rows['classname'] = $class_title;
                    $rows['status'] = $special_request_status;
                    $rows['replied'] = $replied;
                     $rows['centerreply'] = $special_request_replied;
                    $rows['replydate'] = $finalreplydate;
                    

                    array_push($response['request_others_response'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
               
                array_push($response['request_others_response']);
            }
            echo json_encode($response);
        }
    }

    public function Parent_GetTemperature() {
         $data = json_decode(file_get_contents("php://input"), true);
         $parentid = $data['userid'];
         $childid = $data['childid'];
         $index = $data['index'];
         $limit = $data['count'];
         $parentarray = array('parentid' => $parentid,'childid'=>$childid, 'index' => $index, 'count' => $limit);
         $arr_parentpost_data = $parentarray;
         if (!empty($arr_parentpost_data)) {
                $fetch_parent_portfolio = $this->login_model->getParentChildTemperature($arr_parentpost_data);
                $response['temperature']= array();
                $totalrows = $fetch_parent_portfolio['total_records'];
            if ($totalrows > 0) {
                foreach ($fetch_parent_portfolio["temperature_list"] as $view_portfolio_list) {
                    extract($view_portfolio_list);
                    $rows = array();
                    $rows['temperature'] = $temperature_of_day;
                    $rows['remarks'] = $child_remark_of_day;
                    $rows['date']= $child_signin_date;

                    array_push($response['temperature'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                
            //    $rows = array();
            //    $rows['portfolioid'] = "";
            //        $rows['temperature'] = '';
            //        $rows['date']= '';                
            //    array_push($response['temperature'], $rows);
                array_push($response['temperature']);
            }
            echo json_encode($response);
         }
        
    }

    public function Parent_GetAttendance() {
         $data = json_decode(file_get_contents("php://input"), true);
         $parentid = $data['userid'];
         $childid = $data['childid'];
         $index = $data['index'];
         $limit = $data['count'];
         $parentarray = array('parentid' => $parentid,'childid'=>$childid, 'index' => $index, 'count' => $limit);
         $arr_parentpost_data = $parentarray;
         if (!empty($arr_parentpost_data)) {
                $fetch_parent_portfolio = $this->login_model->getParentChildAttendance($arr_parentpost_data);
                $response['attendance']= array();
                $totalrows = $fetch_parent_portfolio['total_records'];
            if ($totalrows > 0) {
                foreach ($fetch_parent_portfolio["attendance_list"] as $view_portfolio_list) {
                    extract($view_portfolio_list);
                    $rows = array();
                    $rows['attendanceid'] = $child_attendance_id;
                    $rows['date']= $child_signin_date;
                    $rows['signintime']= $child_sign_in_time;
                    $rows['signouttime']= $child_sign_out_time;
                    $rows['visualhealthcheckup ']= $visual_health_check;
                    $rows['followup']= $followup_status;
                    $rows['status']= $status_for_the_day;
                    array_push($response['attendance'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $rows = array();
                 $rows['attendanceid'] =  '';
                    $rows['date']= '';
                    $rows['signintime']=  '';
                    $rows['signouttime']=  '';
                    $rows['visualhealthcheckup ']=  '';
                    $rows['followup']= '';
                    $rows['status']=  '';      

                array_push($response['attendance']);
            }
            echo json_encode($response);
         }
        
    }
   

    public function Staff_CreateAnnouncement() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $title = $datas['title'];
        $description = $datas['description'];
        $category = $datas['category'];
        $classids = $datas['classids'];
        if ($title != '' && $classids != '' && $staffid != '') {
            $sql = "Insert into tbl_announcements (announcement_title,announcement_desc,event_catid,class_id,announcement_date_received,announcement_updated_by_id)values('$title','$description','$category','$classids',now(),'$staffid')";
            $login_staff = $this->db->query($sql);
            if ($login_staff) {
                $response['response'] = '1';
            } else {
                $response['response'] = '0';
            }
        } else {
            $response['response'] = '0';
        }
        echo json_encode($response);
    }

    public function Staff_SendReplyForSpecialRequest() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $requestid = $datas['requestid'];
        $userid = $datas['userid'];
        $status = $datas['status'];
        $reply = $datas['reply'];
        if ($requestid != '' && $userid != '' && $status != '' && $reply != '') {
           $sqlreq = "Update tbl_child_special_request SET special_request_replied='$reply',special_request_status='$status',special_request_reply_date=now(),request_replyid='$userid' where special_request_id ='$requestid'";
            $replyreq = $this->db->query($sqlreq);
            if ($replyreq) {
                $response['response'] = '1';
            } else {
                $response['response'] = '0';
            }
        } else {
            $response['response'] = '0';
        }
        echo json_encode($response);
    }

    public function Staff_GetAnnouncementListing($id = '') {

        if ($id == '') {
            $datas = json_decode(file_get_contents("php://input"), true);
            $staffid = $datas['userid'];
            $index = $datas['index'];
            $limit = $datas['count'];
            $staffarray = array('staffid' => $staffid, 'index' => $index, 'count' => $limit);
            $arr_staff_login_post_data = $staffarray;
            if (!empty($arr_staff_login_post_data)) {
                $arr_staff_announcements = $this->login_model->getStaffAnnouncements($arr_staff_login_post_data);
                $response['response'] = '1';
                $response['announcements'] = array();
                $totalrows = $arr_staff_announcements['total_announcement'];
                header('Content-Type: application/json; charset=utf-8');
                if ($totalrows > 0) {
                    foreach ($arr_staff_announcements["announcement_list"] as $view_announcement_list) {
                        extract($view_announcement_list);
//                        print_r($view_announcement_list);
                        $createddate = date('Y-M-d', strtotime($announcement_date_received));
                        $showdate = date('d M Y', strtotime($announcement_date_received));
                        $createdtime = date('g:i A', strtotime($announcement_date_received));
                        $nameOfDay = date('l', strtotime($createddate));
                        $appdate = $nameOfDay . ',' . $showdate . ' ' . $createdtime;
                        $rows = array();
                        $rows['announcementid'] = $announcement_id;
                        $rows['title'] = $announcement_title;
                        $rows['description'] = $announcement_desc;
                        $rows['category'] = $event_title;
                        $rows['dateandtime'] = $appdate;
                        $rows['classids'] = $class_id;

                        array_push($response['announcements'], $rows);
                    }
                } else {
                    array_push($response['announcements']);
                }
                echo json_encode($response);
            }
        } else {
            $requestid = $id;
            $requestarray = array('request_id' => $requestid);
            $arr_request_post_data = $requestarray;
            if (!empty($arr_request_post_data)) {
                $arr_requests_details = $this->login_model->getannouncementsbyid($requestid);
                $response['announcements'] = array();
                $totalrows = $arr_requests_details['total_requests'];
//                echo $totalrows.'svasams';
//                die();
                if ($totalrows > 0) {
                    foreach ($arr_requests_details["requests_details"] as $view_requests_id) {
                        extract($view_requests_id);
//                    print_r($view_requests_id);
                        $rows = array();
                        $rows['requestid'] = $special_request_id;
                        $rows['from_name'] = $special_request_requester_name;
                        $rows['child_name'] = $child_name;
                        $rows['centre_name'] = $centre_name;
                        $rows['class'] = $class_title;
                        $rows['created_date'] = date('d-M-Y', strtotime($special_request_date_received));

                        array_push($response['announcements'], $rows);
//                print_r($view_portfolio_list);
                    }
                } else {
                   

                    array_push($response['special_requests']);
                }
                echo json_encode($response);
            }
        }
    }
    public function Parent_GetAnnouncementListing() {

         
            $datas = json_decode(file_get_contents("php://input"), true);
            $staffid = $datas['userid'];
            $index = $datas['index'];
            $limit = $datas['count'];

            $staffarray = array('staffid' => $staffid, 'index' => $index, 'count' => $limit);
            $arr_staff_login_post_data = $staffarray;
            
            
             $sqljob = "select child_id from tbl_parent where parent_id='$staffid'";
            $queryjb = $this->db->query($sqljob);
            $sessrole = $queryjb->result();
            foreach ($sessrole as $val) {
                $child_id = $val->child_id;
            }
             $sqlclass = "select class_id from tbl_child where child_id='$child_id'";
            $querycls = $this->db->query($sqlclass);
            $sesscls = $querycls->result();
            foreach ($sesscls as $value) {
                $class_id = $value->class_id;
            }
            
            if (!empty($arr_staff_login_post_data)) {

              $arr_staff_announcements = $this->login_model->getParentAnnouncements($arr_staff_login_post_data, $class_id);

                $response['response'] = '1';
                $response['announcements'] = array();
                $totalrows = $arr_staff_announcements['total_announcement'];
                header('Content-Type: application/json; charset=utf-8');
                if ($totalrows > 0) {
                    foreach ($arr_staff_announcements["announcement_list"] as $view_announcement_list) {
                        extract($view_announcement_list);

                        $createddate = date('Y-M-d', strtotime($announcement_date_received));
                        $showdate = date('d M Y', strtotime($announcement_date_received));
                        $createdtime = date('g:i A', strtotime($announcement_date_received));
                        $nameOfDay = date('l', strtotime($createddate));
                        $appdate = $nameOfDay . ',' . $showdate . ' ' . $createdtime;
                        $rows = array();
                        $rows['announcementid'] = $announcement_id;
                        $rows['title'] = $announcement_title;
                        $rows['description'] = $announcement_desc;
                        $rows['category'] = $event_title;
                        $rows['dateandtime'] = $appdate;


                        array_push($response['announcements'], $rows);
                    }
                } else {
                    $response['response'] = '1';
                   array_push($response['announcements']);
                }
                echo json_encode($response);
            }
        } 
    

    public function Staff_EditProfile() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $name = $datas['name'];
        $password = md5($datas['password']);
        $dt = $datas['photodata'];
        
        $profile_details = $this->login_model->getstaffphoto($staffid);
        // print_r($profile_details);
        foreach ($profile_details["staff_list"] as $view_stafflist) {
            extract($view_stafflist);
            $staff_photo;
        }

        if ($dt != '') {
            $randno = rand(1000, 9999);
            $imgname = $staffid . '_' . $randno . ".jpg";
            $binary = base64_decode($dt);
            header('Content-Type: image/jpeg; charset=utf-8');
            $staff_folder = STAFF_PHOTO_PATH . $imgname;
            $file = fopen($staff_folder, "wb");
            fwrite($file, $binary);
            fclose($file);
            $ins = ",staff_photo='$imgname'";

                $thumb_destination = STAFF_PHOTO_THUMB_PATH . $imgname;
                createthumb($staff_folder, $thumb_destination, 200, 200);

                unlink(STAFF_PHOTO_PATH . $staff_photo);
                unlink(STAFF_PHOTO_THUMB_PATH . $staff_photo);
        } else {
            $ins = "";
        }

        if ($staffid != '' && $name != '' && $password != '') {
            $sqlprof = "Update tbl_staff set staff_name='$name',staff_password='$password' $ins where staff_id = '$staffid'";
            $edit_prof = $this->db->query($sqlprof);
        }

        if ($edit_prof) {
            $response['response'] = '1';
        } else {
            $response['response'] = '0';
        }
        echo json_encode($response);
    }

 public function Parent_EditProfile(){
        $data = json_decode(file_get_contents("php://input"),true);
        $parentid = $data['userid'];
        $name = $data['name'];
        $password = md5($data['password']);
        $dt_photo = $data['photodata'];
        $parent_profile_details = $this->login_model->getParentphoto($parentid);
        // print_r($parent_profile_details);
        foreach ($parent_profile_details["parent_list"] as $view_plist) {
            extract($view_plist);
           $photourl;
        }
        if($dt_photo !=''){
            $randno = rand(1000, 9999);
            $imgname = $parentid . '_' . $randno . ".jpg";
            $binary = base64_decode($dt_photo);
            header('Content-Type: image/jpeg; charset=utf-8');
            $parent_folder = PARENT_PHOTO_PATH . $imgname;
            $file = fopen($parent_folder, "wb");
            fwrite($file, $binary);
            fclose($file);
            $ins = ",photourl='$imgname'";

                $thumb_destination = PARENT_PHOTO_THUMB_PATH . $imgname;
                createthumb($parent_folder, $thumb_destination, 200, 200);

                unlink(PARENT_PHOTO_PATH . $photourl);
                unlink(PARENT_PHOTO_THUMB_PATH . $photourl);
        }else{
             $ins = "";
        }
        if ($parentid != '' && $name != '' && $password != '') {
            $sqlprof = "Update tbl_parent set parent_name='$name',password='$password' $ins where parent_id = '$parentid'";
            $edit_prof = $this->db->query($sqlprof);
        }

        if ($edit_prof) {
            $response['response'] = '1';
        } else {
            $response['response'] = '0';
        }
        echo json_encode($response);
 }

    public function Parent_Feedback(){
        $data = json_decode(file_get_contents("php://input"), true);
        
        $parentid = $data['userid'];
        $feed_title = $data['title'];
        $feed_details = $data['details'];
        $parentfeedarray= array('userid'=>$parentid, 'feedback_title'=>$feed_title, 'feedback_details'=>$feed_details);
      
        $parent_feedback_ins = $this->login_model->SaveParentFeedback($parentfeedarray);
         $arr_parent = array();
        if ($parent_feedback_ins) { 
            $arr_parent["msg"] = 'Feedback saved successfully';
            $sess_parent = array("response" => '1', "parent_feed_data" => $arr_parent);
                echo json_encode($sess_parent);
                die();
        }else{
            $arr_parent["msg"] = 'Feedback not saved';
            $sess_parent = array("response" => '0', "parent_feed_data" => $arr_parent);
            echo json_encode($sess_parent);
            die();
        }

    }
     public function Staff_UploadPortolioPhoto() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $dt = $datas['filedata'];
         $response = array();
         header('Content-Type: application/json; charset=utf-8');
       
        if ($dt != '') {
            $randno = rand(1000, 9999);
            $imgname = $randno . ".jpg";

            $binary = base64_decode($dt);
            
            
            $port_folder = CHILD_LEARNING_PORTFOLIO_UPLOAD . $imgname;
            $file = fopen($port_folder, "wb");
            fwrite($file, $binary);
            fclose($file);

            $thumb_destination = CHILD_LEARNING_PORTFOLIO_THUMB_UPLOAD . $imgname;
            createthumb($port_folder, $thumb_destination, 200, 200);

            $file_details = $this->login_model->insertportfiles($imgname);
            if ($file_details != '0') {
                $response['response'] = '1';
                $response['fileid'] = "$file_details";
            } else {
                $response['response'] = '0';
                $response['fileid'] = '';
            }
        } else {
            $response['response'] = '0';
            $response['fileid'] = '';
        }

        echo json_encode($response);
    }
     public function Staff_CreatePortfolio() {
        $datas = json_decode(file_get_contents("php://input"), true);
         header('Content-Type: application/json; charset=utf-8');
        $staffid = $datas['userid'];
      //  $files = $datas['files'];
        $fileid = $datas['fileid'];
        $centreid = $datas['centreid'];
        $classid = $datas['classid'];
        $piesid = $datas['piesid'];
        $childid = $datas['childid'];
        $venue = $datas['venue'];
        $date = $datas['date'];
        $activedesc = $datas['activitydescription'];
        $cognitive = $datas['cognitive'];
        $socials = $datas['socials'];

        $portins_details = $this->login_model->createportfolio($datas);
        $updport = $this->login_model->uploadport($datas, $portins_details, $fileid, $activedesc);

        if ($updport != '0') {
            $response['response'] = '1';
        } else {
            $response['response'] = '0';
        }

        echo json_encode($response);
    }
    
    
    
    
    
    
     public function Staff_GetAllthePortfolios() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $staffid = $datas['userid'];
        $index = $datas['index'];
        $limit = $datas['count'];
        $staffarray = array('staffid' => $staffid, 'index' => $index, 'count' => $limit);
        $arr_staff_login_post_data = $staffarray;
         header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_portfolios = $this->login_model->getStaffportfolio($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['portfolio_list'] = array();
            $totalrows = $arr_staff_portfolios['total_portfolio'];
            if ($totalrows > 0) {

                foreach ($arr_staff_portfolios["portfolio_list"] as $view_portfolios_list) {
                    extract($view_portfolios_list);
//                    print_r($view_portfolios_list);
                    $createddate = date('Y-M-d', strtotime($portfolio_created_date));
                    $showdate = date('d M Y', strtotime($portfolio_created_date));
                    $createdtime = date('g:i A', strtotime($portfolio_created_date));
                    $nameOfDay = date('l', strtotime($createddate));
                    $appdate = $nameOfDay . ',' . $showdate . ' ' . $createdtime;
                    $rows = array();
                    $rows['portfolioid'] = $portfolio_id;
                    $rows['staffid'] = $portfolio_updated_by_id;
                    $rows['centerid'] = $centre_id;
                    $rows['centername'] = $centre_name;
                    $rows['classid'] = $class_id;
                    $rows['classname'] = $class_title;
                     $rows['childid'] = array($childis);
                     $arr_staff_childnames = $this->login_model->getChildnames($childis);
                     $totalchilds = $arr_staff_childnames['total_childs'];
            if ($totalchilds > 0) {

                foreach ($arr_staff_childnames["child_lists"] as $viewchild_list) {
                    extract($viewchild_list);
                    $child_names .= $child_name . ',';
                }
                 $child_names = substr(trim($child_names), 0, -1);
                   $rows['childname'] = array($child_names);
                     $child_names = "";
            }else{
                $rows['childname'] = array('');
            }
                   
                    if ($image != '') {
                        $photourl = CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $image;
                        $rows['photourl'] = array($photourl);
                    } else {
                        $photourl = $portfolio_files;
                        $tmp = explode(',', $portfolio_files);
                         $cntport = count($tmp);
                        $sqlprof = "select * from tbl_portfolio_file where file_id IN ($portfolio_files)";
                        $edit_prof = $this->db->query($sqlprof);
                       
                        if ($edit_prof->num_rows()) {
                        $portvals = $edit_prof->result_array();
                         $i=1;
                        foreach ($portvals as $view_portf_list) {
                            extract($view_portf_list);
                           
                           if($i == $cntport){
                                     $imgpath .= CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $file_img . ',';
                                }else{
                                     $imgpath .= CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $file_img . ',';
                                }
                                 $i++;
                        }
                        $imgpath = substr(trim($imgpath), 0, -1);
                        $rows['photourl'] = array($imgpath);
                        $imgpath = "";
                        }else{
                              $rows['photourl'] = array('');
                        }
                    }
                    $rows['venue'] = $venue;
                    $rows['date'] = $showdate;
                    $rows['activitydescription'] = htmlspecialchars_decode($description);
                    $rows['cognitive'] = $cognitive;
                    $rows['socials'] = $socials;
                    $rows['createddateandtime'] = $appdate;
                    array_push($response['portfolio_list'], $rows);
                }
            } else {
                 
                $response['response'] = '1';
            
               
                array_push($response['portfolio_list']);
            }
            echo json_encode($response);
        }
    }
    
       public function Staff_DeleteFile() {
        $datas = json_decode(file_get_contents("php://input"), true);
         $userid = $datas['userid'];
        $fileid = $datas['fileid'];
        $upldarray = array('userid' => $userid, 'fileid' => $fileid);
        $arr_ataffdel_data = $upldarray;
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_ataffdel_data)) {
            $delete_uploads = $this->login_model->updatedelport($arr_ataffdel_data);
            if ($delete_uploads == '1') {
                $response['response'] = '1';
            } else {
                $response['response'] = '0';
            }
        }
        echo json_encode($response);
    }
    
    public function Parent_GetPortfolio() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $parentid = $datas['userid'];
         $childid = $datas['childid'];
         $index = $datas['index'];
         $limit = $datas['count'];
         $parentarray = array('parentid' => $parentid,'childid'=>$childid, 'index' => $index, 'count' => $limit);
         $arr_parentpost_data = $parentarray;
         header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_parentpost_data)) {
            $arr_staff_portfolios = $this->login_model->getParentPortfolio($arr_parentpost_data);
            $response['response'] = '1';
            $response['portfolio_list'] = array();
            $totalrows = $arr_staff_portfolios['total_portfolio'];
            if ($totalrows > 0) {

                foreach ($arr_staff_portfolios["portfolio_list"] as $view_portfolios_list) {
                    extract($view_portfolios_list);
//                    print_r($view_portfolios_list);
                    $createddate = date('Y-M-d', strtotime($portfolio_created_date));
                    $showdate = date('d M Y', strtotime($portfolio_created_date));
                    $createdtime = date('g:i A', strtotime($portfolio_created_date));
                    $nameOfDay = date('l', strtotime($createddate));
                    $appdate = $nameOfDay . ',' . $showdate . ' ' . $createdtime;
                    $rows = array();
                    $rows['portfolioid'] = $portfolio_id;
                    $rows['staffid'] = $portfolio_updated_by_id;
                    $rows['centerid'] = $centre_id;
                    $rows['centername'] = $centre_name;
                    $rows['classid'] = $class_id;
                    $rows['classname'] = $class_title;
                    $rows['childid'] = $child_id;
                    $rows['childname'] = $child_name;
                   
                    if ($image != '') {
                        $photourl = CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $image;
                        $rows['photourl'] = array($photourl);
                    } else {
                        $photourl = $portfolio_files;
                        $tmp = explode(',', $portfolio_files);
                        $cntport = count($tmp);
                     
                        $sqlprof = "select * from tbl_portfolio_file where file_id IN ($portfolio_files)";
                        $edit_prof = $this->db->query($sqlprof);
                        $numrows = $edit_prof->num_rows();
                        if ($edit_prof->num_rows()) {
                            $portvals = $edit_prof->result_array();
                           $i=1;
                            foreach ($portvals as $view_portf_list) {
                                extract($view_portf_list);
                                if($i == $cntport){
                                     $imgpath .= CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $file_img . ',';
                                }else{
                                     $imgpath .= CHILD_LEARNING_PORTFOLIO_THUMB_WEB . $file_img . ',';
                                }
                               
                            $i++;
                                
                            }
                        $imgpath = substr(trim($imgpath), 0, -1);
                        $rows['photourl'] = array($imgpath);
                        $imgpath = "";
                        } else {
                            $rows['photourl'] = array('');
                        }
                    }
                    $rows['venue'] = $venue;
                    $rows['date'] = $showdate;
                    $rows['activitydescription'] = htmlspecialchars_decode($description);
                    $rows['cognitive'] = $cognitive;
                    $rows['socials'] = $socials;
                    $rows['createddateandtime'] = $appdate;
                    array_push($response['portfolio_list'], $rows);
                }
            } else {
                $response['response'] = '1';
               
                array_push($response['portfolio_list']);
            }
            echo json_encode($response);
        }
    }
    public function Staff_Feedback(){
        $data = json_decode(file_get_contents("php://input"), true);
        
        $parentid = $data['userid'];
        $feed_title = $data['title'];
        $feed_details = $data['details'];
        $parentfeedarray= array('userid'=>$parentid, 'feedback_title'=>$feed_title, 'feedback_details'=>$feed_details);
      header('Content-Type: application/json; charset=utf-8');
        $parent_feedback_ins = $this->login_model->SaveStaffFeedback($parentfeedarray);
         $arr_parent = array();
        if ($parent_feedback_ins) { 
            $response['response'] = '1';
                echo json_encode($response);
                die();
        }else{
            $response['response'] = '0';
           echo json_encode($response);
            die();
        }

    }
    
     public function TermsAndConditions(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        header('Content-Type: application/json; charset=utf-8');
        $fetch_terms = $this->login_model->getTermsandConditions();
         $response['response'] = '1';
                $response['termsresponse']= array();
                $totalrows = $fetch_terms['total_records'];
            if ($totalrows > 0) {
                
                foreach ($fetch_terms["termslist"] as $view_terms) {
                    extract($view_terms);
                    $rows = array();
                    $rows['terms'] = $terms;
                       array_push($response['termsresponse'], $rows);
                }
                
            }else{
                $response['response'] = '0';
                    $rows = array();
                    $rows['terms'] = '';
                       array_push($response['termsresponse'], $rows);
            }
             echo json_encode($response);
    }

     public function About(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        header('Content-Type: application/json; charset=utf-8');
        $fetch_terms = $this->login_model->getAboutContent();
        $response['response'] = '1';
                $response['aboutresponse']= array();
                $totalrows = $fetch_terms['total_records'];
            if ($totalrows > 0) {
                foreach ($fetch_terms["aboutdetail"] as $view_terms) {
                    extract($view_terms);
                    $rows = array();
                    $rows['about'] = $about;
                       array_push($response['aboutresponse'], $rows);
                }
                
            }else{
                $response['response'] = '0';
                    $rows = array();
                    $rows['about'] = '';
                       array_push($response['aboutresponse'], $rows);
            }
             echo json_encode($response);
    }
    
    public function Staff_TermsAndConditions(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        header('Content-Type: application/json; charset=utf-8');
        $fetch_terms = $this->login_model->getstaffTermsandConditions();
         $response['response'] = '1';
                $response['termsresponse']= array();
                $totalrows = $fetch_terms['total_records'];
            if ($totalrows > 0) {
                
                foreach ($fetch_terms["termslist"] as $view_terms) {
                    extract($view_terms);
                    $rows = array();
                    $rows['terms'] = $terms;
                       array_push($response['termsresponse'], $rows);
                }
                
            }else{
                $response['response'] = '0';
                    $rows = array();
                    $rows['terms'] = '';
                       array_push($response['termsresponse'], $rows);
            }
             echo json_encode($response);
    }

     public function About_StaffApp(){
        $data = json_decode(file_get_contents("php://input"), true);
        $parentid = $data['userid'];
        header('Content-Type: application/json; charset=utf-8');
        $fetch_terms = $this->login_model->getstaffAboutContent();
        $response['response'] = '1';
                $response['aboutresponse']= array();
                $totalrows = $fetch_terms['total_records'];
            if ($totalrows > 0) {
                foreach ($fetch_terms["aboutdetail"] as $view_terms) {
                    extract($view_terms);
                    $rows = array();
                    $rows['about'] = $about;
                       array_push($response['aboutresponse'], $rows);
                }
                
            }else{
                $response['response'] = '0';
                    $rows = array();
                    $rows['about'] = '';
                       array_push($response['aboutresponse'], $rows);
            }
             echo json_encode($response);
    }
    
      public function Staff_UploadFiles() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $userid = $datas['userid'];
        $mediatitle = $datas['mediatitle'];
        $extension = $datas['extension'];
        $dt = $filedata = $datas['filedata'];
        $profarray = array('userid' => $userid, 'mediatitle' => $mediatitle);
        $arr_prof_data = $profarray;
        header('Content-Type: application/json; charset=utf-8');
        if ($dt != '') {
            $randno = rand(1000, 9999);
            $binary = base64_decode($dt);
            $imgname = $randno . '.' . $extension;
            $port_folder = LIBRARY_DOCUMENT_PATH . $imgname;
            $file = fopen($port_folder, "wb");
            fwrite($file, $binary);
            fclose($file);


            $thumb_destination = LIBRARY_DOCUMENT_PATH . $imgname;
            createthumb($port_folder, $thumb_destination, 200, 200);

            $file_details = $this->login_model->insertuploadfiles($imgname, $arr_prof_data);


            if ($file_details != '0') {
                $response['response'] = '1';
                
            } else {
                $response['response'] = '0';
                $response['fileid'] = '';
            }
        } else {
            $response['response'] = '0';
            $response['fileid'] = '';
        }
        echo json_encode($response);
    }
    
    
    
    public function Staff_GetStudentProfileAndCheckin() {
        $datas = json_decode(file_get_contents("php://input"), true);

        $userid = $datas['userid'];
        $childid = $datas['studentid'];



        $sqlchk5 = "select parent_id,parent_name from tbl_parent where child_id = '$childid'";
        $querychk5 = $this->db->query($sqlchk5);
        $numrows5 = $querychk5->num_rows();
        if ($numrows5 > 0) {
            $sessrole5 = $querychk5->result();
            foreach ($sessrole5 as $val5) {
                $parentid = $val5->parent_id;
                $parentname = $val5->parent_name;
            }
        } else {
            $parentid = '';
            $parentname = '';
        }


        $sqlchk = "select child_display_id,child_name,centre_id,child_photo,class_id from tbl_child where child_id = '$childid'";
        $querychk = $this->db->query($sqlchk);
        $numrows = $querychk->num_rows();
        $sessrole1 = $querychk->result();
        foreach ($sessrole1 as $val1) {
            $childdisplayid = $val1->child_display_id;
            $childname = $val1->child_name;
            $centreid = $val1->centre_id;
            $classid = $val1->class_id;
            $childimg = $val1->child_photo;
            $photourl = VIEW_DIR . 'chlid/thumb/' . $childimg;

            $sqlchk1 = "select centre_name from tbl_centre where centre_id = '$centreid'";
            $querychk1 = $this->db->query($sqlchk1);
            $numrows1 = $querychk1->num_rows();
            $sessrole2 = $querychk1->result();
            foreach ($sessrole2 as $val2) {
                $centrename = $val2->centre_name;
            }
            $sqlchk2 = "select class_title from tbl_class where class_id = '$classid'";
            $querychk2 = $this->db->query($sqlchk2);
            $numrows2 = $querychk2->num_rows();
            $sessrole3 = $querychk2->result();
            foreach ($sessrole3 as $val3) {
                $classname = $val3->class_title;
            }
        }
        
        header('Content-Type: application/json; charset=utf-8');
        $response['response'] = '1';
        $response['signindetails'] = array();


        $profarray = array('userid' => $userid, 'childid' => $childid, 'class_id' => $classid, 'centre_id' => $centreid);
        $arr_prof_data = $profarray;
        if (!empty($arr_prof_data)) {
            $student_sign = $this->login_model->stdsignin($arr_prof_data);

            if ($student_sign != '0') {
                $sqlchk4 = "select child_sign_in_time from tbl_child_attendance where child_attendance_id = '$student_sign'";
                $querychk4 = $this->db->query($sqlchk4);
                $numrows4 = $querychk4->num_rows();
                $sessrole4 = $querychk4->result();
                foreach ($sessrole4 as $val4) {
                    $signintime = $val4->child_sign_in_time;
                    $showtime = date('h:i A', strtotime($signintime));
                }
                $rows = array();
                $rows['attendanceid'] = "$student_sign";
                $rows['studentid'] = "$childid";
                $rows['studentname'] = "$childname";
                $rows['parentid'] = "$parentid";
                $rows['parentname'] = "$parentname";
                $rows['centerid'] = "$centreid";
                $rows['centername'] = "$centrename";
                $rows['photourl'] = "$photourl";
                $rows['childdisplayid'] = "$childdisplayid";
                $rows['classid'] = "$classid";
                $rows['classname'] = "$classname";
                $rows['checkintime'] = "$showtime";
                array_push($response['signindetails'], $rows);
            } else {
                $response['response'] = '0';
                array_push($response['signindetails']);
            }
        }
        echo json_encode($response);
    }
    
    public function Staff_StudentCheckout() {
        $data = json_decode(file_get_contents("php://input"), true);
        $staffid = $data['userid'];
        $studentid = $data['studentid'];
        $attendanceid = $data['attendanceid'];
  header('Content-Type: application/json; charset=utf-8');
        $insertarray = array('userid' => $staffid, 'student_id' => $studentid, 'attendance_id' => $attendanceid);

        $staff_chkout = $this->login_model->StaffCheckout($insertarray);
        $arr_chkout = array();
        if ($staff_chkout) {
            $arr_chkout["msg"] = 'Child sig saved successfully';
            $sess_parent = array("response" => '1');
            echo json_encode($sess_parent);
            die();
        } else {
            $arr_chkout["msg"] = 'Feedback not saved';
            $arr_chkout = array("response" => '0');
            echo json_encode($sess_parent);
            die();
        }
    }
    
     public function Get_All_Staffs() {
        $datas = json_decode(file_get_contents("php://input"), true);
        $parentid = $datas['userid'];
        $index = $datas['index'];
        $limit = $datas['count'];
        $staffarray = array('userid' => $parentid, 'index' => $index, 'count' => $limit);

        $arr_staff_login_post_data = $staffarray;
        header('Content-Type: application/json; charset=utf-8');
        if (!empty($arr_staff_login_post_data)) {
            $arr_staff_centre = $this->login_model->GetallStaff($arr_staff_login_post_data);
            $response['response'] = '1';
            $response['staffs'] = array();
            $totalrows = $arr_staff_centre['total_staff'];
            if ($totalrows > 0) {
                foreach ($arr_staff_centre["staff_list"] as $view_staff_centre) {
                    extract($view_staff_centre);
                    $rows = array();
                    $rows['staffid'] = $staff_id;
                    $rows['staffname'] = $staff_name;
                    $rows['staffphoto'] = VIEW_DIR . 'staff/thumb/' . $staff_photo;
                    $rows['quickbloxid'] = $quickblox_id;


                    array_push($response['staffs'], $rows);
//                print_r($view_portfolio_list);
                }
            } else {
                $rows = array();
                $response['response'] = '0';
                array_push($response['centres']);
            }
            echo json_encode($response);
        }
    }
}